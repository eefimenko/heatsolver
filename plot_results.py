#!/usr/bin/python
import matplotlib.pyplot as plt

def read_file(file):
    f = open(file)
    n = []
    r = []
    for line in f:
        print line
        tmp = line.split()
        n.append(int(tmp[0]))
        r.append(float(tmp[1]))
    return n,r

def main():
   
    fig = plt.figure(figsize=(6,6))
    ax = fig.add_subplot(1,1,1)
    n1, r1 = read_file('ftcs.dat')
    n2, r2 = read_file('crni.dat')
    n3, r3 = read_file('adi.dat')
    
    ax.plot(n1, r1, 'r', label='FTCS')
    ax.plot(n2, r2, 'g', label='Cr-Ni')
    ax.plot(n3, r3, 'b', label='ADI')
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_ylabel('time,s')
    ax.set_xlim([7, 200])
    ax.set_xlabel('number of steps')
    ax.set_title('Time of execution of one iteration')
    plt.legend(loc='lower right')
    
    plt.tight_layout()
    plt.savefig('result.png')
    plt.close()

if __name__ == "__main__":
    main()
