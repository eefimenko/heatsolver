#ifndef HEADER
#define HEADER

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<sys/time.h>
#include<math.h>
#include<float.h>
#include<unistd.h>
#include<string.h>

#define NPRE 2 // Number of smoothing GS iteartions before restriction
#define NPOST 2 //Number of smoothing GS iteartions after interpolation

#define TRUE 1
#define FALSE 0

// Geometry boundary conditions
#define DIRICHLET 0
#define PERIODIC  1

#define DIFFUSION 11
#define ADVECTION 12
#define TEST 13

#define FTCS 0
#define CRNI 1
#define ADI  2

#define SOR 21
#define RREF 22
#define CG 23
#define PCG 24
#define MG 25
#define MG_OLD 26

#define FMG 31
#define VSWEEP 32

#define JACOBI_PRE 41
#define SSOR_PRE 42
#define MG_PRE 43

#define INDEX(f,i,j,k) ((i)+ f.nx*(j) + f.nxy*(k))
/*#define INDEX_P(f_p,i,j,k) ((i)+ (f_p)->nx*(j) + (f_p)->nxy * (k))
#define DX(f_p) 1
#define DY(f_p) (f_p)->ny
#define DZ(f_p) (f_p)->nxy*/
#define INDEX_P(f_p,i,j,k) ((j)+ (f_p)->ny*(i) + (f_p)->nxy * (k))
#define DX(f_p) (f_p)->ny
#define DY(f_p) 1
#define DZ(f_p) (f_p)->nxy
#define IDX(m_p,i,j) ((j)+ (m_p)->ny*(i))

typedef struct Parameters_{
    int bc; // boundary condition: constant, periodic
    int bcx; // boundary condition in x dimension: constant, periodic
    int bcy; // boundary condition in y dimension: constant, periodic
    int bcz; // boundary condition in z dimension: constant, periodic
    int solver; // Type of solver: FTCS, Crank-Nikolson, ADI
    int msolver; // Type of matrix solver: SOR, RREF
    int equation; // Type of equation: diffusion, advection
    int mgcycle; // Type of cycle for MultiGrid solver: FMG, VSWEEP
    int preconditioner; // Type of preconditioner for CG: JACOBI_PRE, SSOR_PRE, MG_PRE
    double lx; // size of box in x dimension
    double ly; // size of box in y dimension
    double lz; // size of box in z dimension
    double vx; // velocity in x dimension
    double vy; // velocity in y dimension
    double vz; // velocity in z dimension
    int nx; // number  steps in x dimension
    int ny; // number of steps in y dimension
    int nz; // number of steps in z dimension
    int num; // number of iterations
    int num_10; // print output after each 10% of iterations
    int save; // step between figure output
    int prof; // step between profile save
    double dt; // time step
    double d; // diffusivity constant
    double eps; // termination criterion: tolerance
    int maxiteration; // termination criterion: maxiterations
    double w; // SOR parameter
    double x0; // center(x) of initial gaussian distribution
    double y0; // center(y) of initial gaussian distribution
    double z0; // center(z) of initial gaussian distribution
    double a; //width of initial distribution in x dimension
    double b; //width of initial distribution in y dimension
    double c; //width of initial distribution in z dimension
    double sx0; // center(x) of source  gaussian distribution
    double sy0; // center(y) of source  gaussian distribution
    double sz0; // center(z) of source  gaussian distribution
    double sa; //width of source  distribution in x dimension
    double sb; //width of source  distribution in y dimension
    double sc; //width of source  distribution in z dimension
    double samp; //source amlitude
    double t0; // background temperature
    double t1; // amplitude of the initial distribution
    double tr; // amplitude of the random component(noise)
    double txmin; // temperature at plane x = xmin
    double txmax; // temperature at plane x = xmax
    double tymin; // temperature at plane y = ymin
    double tymax; // temperature at plane y = ymax
    double tzmin; // temperature at plane z = zmin
    double tzmax; // temperature at plane z = zmax
} Parameters;

typedef struct Geometry_{
    int bc; // boundary condition
    int ndim; // Number of dimensions
    double dx; // spatial step in x dimension
    double dy; // spatial step in y dimension
    double dz; // spatial step in z dimension
    int nx; // number of steps in x dimension
    int ny; // number of steps in y dimension
    int nz; // number of steps in z dimension
    double ax; // coefficient for numerical schemes for d^2/dx^2
    double ay; // coefficient for numerical schemes for d^2/dy^2
    double az; // coefficient for numerical schemes for d^2/dz^2
    double bx; // coefficient for numerical schemes for d/dx
    double by; // coefficient for numerical schemes for d/dy
    double bz; // coefficient for numerical schemes for d/dz
    double b; // coefficient for numerical schemes for source
    int bcx; // boundary condition in x dimension: constant, periodic
    int bcy; // boundary condition in y dimension: constant, periodic
    int bcz; // boundary condition in z dimension: constant, periodic
} Geometry;

typedef struct field_
{
    int nx;
    int ny;
    int nz;
    int nxy;
    double* a;
} field;

typedef struct matrix_
{
    int nx;
    int ny;
    double* a;
} matrix;

typedef struct matrix3_
{
    int n;
    double* a;
    double* b;
    double* c;
    double* u;
} matrix3;

typedef struct Multigrid_{
    int ng; // number of grids in multigrid
    matrix* a; // matrices for multigrids
    field* u1; // field on i-th grid
    field* u10; // auxilary field on i-th grid, that stores initial field value
    field* ures; // auxilary field on i-th grid which stores residual
    int* mult; // this number shows how many times step on i-th grid greater than fine grid step
    int* num; // size of i-th matrix 
    int* gridsize; // number of step in i-th grid
    double** x; //array of vectors for solution on i-th grid
    double** res; //array of vectors for residual on i-th grid
} Multigrid;

void init_multigrid(Multigrid* m, field* u1_p, Geometry* g, Parameters* p);
void destroy_multigrid(Multigrid* m);
int vsweep(Multigrid* m, Geometry* g, Parameters* p, double eps, int nmax);
int fmg(Multigrid* m, Geometry* g, Parameters* p, double eps, int nmax);



typedef struct Preconditioner_{
    int n;
    double w;
    double w2;
    double* pre;
    Multigrid m;
} Preconditioner;

void init_preconditioner(Preconditioner* pre, field* u1_p, matrix* m, double w, Geometry* g, Parameters* p);
void destroy_preconditioner(Preconditioner* pre, Parameters* p);
int swap_field(field* dst, field* src);
void parse_parameters(Parameters *parameters);
void print_parameters(Parameters *parameters);
void read_command_line(int argc, char *argv[], Parameters *parameters);
void print_error(char *message);
void border_print(void);
void center_print(const char *s, int width);
Parameters *init_parameters(void);
Geometry *init_geometry(Parameters *parameters);
double timer(void);
int init_field(field* f_p, int nx, int ny, int nz);
int init_matrix(matrix* m_p, int nx, int ny);
int init_matrix3(matrix3* m_p, int n, double nu);
void destroy_field(field* f_p);
void set_initial_distribution(field* f_p, Geometry* g, Parameters* p);
void save_field(field* f_p, int t, Geometry* g);
void max_avg_value(field* f_p, double* max, double*avg);
void update_diffusion_ftcs(field* f_p, field* fnew_p, Geometry* g, Parameters* p);
void update_advection_ftcs(field* f_p, field* fnew_p, Geometry* g, Parameters* p);
void apply_periodic_bc(field* f_p, Geometry* g);
void apply_constant_bc(field* f_p, Geometry* g, Parameters* p);
void save_profile(field* f_p, int t, Parameters* p, Geometry* g);
void save_data(field* f_p, int t, Parameters* p, Geometry* g, double t1);
void apply_dummy_bc(field* f_p, Geometry* g);
void print_matrix(matrix* m_p);
void destroy_matrix(matrix* m_p);
void destroy_matrix3(matrix3* m_p);
void rref(matrix* m_p);
void solve(matrix3* m_p);
void print_matrix3(matrix3* m_p);
double uran(double a);
void initialize_adi_scheme(matrix3* m_p, double nu, Geometry* g);
void initialize_diffusion_crni_scheme(matrix* m_p, Geometry* g);
void initialize_advection_crni_scheme(matrix* m_p, Geometry* g);
void update_matrix3_with_field(matrix3* m_p, field* f_p, Geometry* g, int dx, double nu, Parameters* p);
void update_diffusion_matrix_with_field(matrix* m_p, field* f_p, Geometry* g, Parameters* p);
void update_advection_matrix_with_field(matrix* m_p, field* f_p, Geometry* g, Parameters* p);
void extract_field_from_matrix3(matrix3* m_p, field* f_p, Geometry* g);
void extract_field_from_matrix(matrix* m_p, field* f_p, Geometry* g);
void print_help();
void print_value(const char* name, double time);
double source(int i, int j, int k, Geometry* g, Parameters* p);
int sor(matrix* m, double* x, double tol, int nmax, double w);
double norm(double* a1, double* a2, int n);
void read_field_from_vector(field* f_p, double* x, Geometry* g);
void write_field_to_vector(field* f_p, double* x, Geometry* g);
double diff_field(field* f1_p, field* f2_p);
double norm_field(field* f1_p);
double* init_vector(int num);
void print_field(field* f_p);
void sum_vectors(double *res, double m1, double m2, double *x1, double *x2, int n);
double ddot(double *x, double *y, int n);
void matvec(double *res, matrix* A, double* vec, int n);
int cg(matrix *A, double *x, double *b, double *p, double *r, double *ap, double rtol, int maxiter);
void print_v(char* name, double* v, int n);
void update_vectors(double *res, double m, double *x, int n);
int pcg(matrix *A, double *x, double* b, double* r, double* z, double* d, double* ap, double rtol, int maxiter, double w, Preconditioner *pre, Geometry* g, Parameters* p);
void mdot(matrix* A, matrix* B, matrix* C);
void inv(matrix* m, matrix* inv, int n);
int zero_matrix(matrix* m_p);
void rstrct(field *uc, field *uf, int nc);
void fill_diffusion_matrix(matrix* m_p, field* f_p, Geometry* g, Parameters* p);
void copy_matrix(matrix *out, matrix *in);
void interp(field *uf, field *uc, int nf);
void addint(field *uf, field *uc, field *res, int nf);
void initialize_diffusion_mg_scheme(matrix* m_p, Geometry* g, int mult);
void update_diffusion_matrix_with_field_mg(matrix* m_p, field* f_p, Geometry* g, Parameters* p, int mult, double source);
void set_initial_distribution_mg(field* f_p, Geometry* g, Parameters* p, double m);
double source_mg(int i, int j, int k, Geometry* g, Parameters* p, int mult);
void residual(matrix* a, double* x, double* res, int n);
void update_diffusion_matrix_mg(matrix* m_p, field* f_p, Geometry* g, Parameters* p);
int copy_field(field* dst, field* src);
void read_vector_from_field(field* f_p, double* x, Geometry* g);
void ssor_pre(Preconditioner* pre, matrix* m, double* b, double* x);
void jacobi_pre(Preconditioner* pre, matrix* m, double* b, double* x);
void mg_pre(Preconditioner* pre, double* b, double* x, Geometry* g, Parameters* p);
int sor_n(matrix* m, double* x, double tol, int nmax, double w);
int copy_vector(double* dst, double* src, int n);
#endif
