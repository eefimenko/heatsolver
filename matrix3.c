#include "header.h"

int init_matrix3(matrix3* m_p, int n, double nu)
{
    m_p->n = n;
    
    if (n == 0)
    {
        m_p->a = NULL;
        return 0;
    }

    m_p->a = (double*)malloc(n*sizeof(double));
    m_p->b = (double*)malloc(n*sizeof(double));
    m_p->c = (double*)malloc(n*sizeof(double));
    m_p->u = (double*)malloc(n*sizeof(double));
    
    if (NULL == m_p->a || NULL == m_p->b || NULL == m_p->c || NULL == m_p->u)
    {
        fprintf(stderr, "Error while allocating memory\n");
	fflush(stderr);
        exit(-1);
    }
    memset(m_p->a, 0, n*sizeof(double));
    memset(m_p->b, 0, n*sizeof(double));
    memset(m_p->c, 0, n*sizeof(double));
    memset(m_p->u, 0, n*sizeof(double));

    return 0;
}

void print_matrix3(matrix3* m_p)
{
    int i,j;
    for (i=0; i<m_p->n; i++)
    {
	for (j=0; j<m_p->n; j++)
	{
	    if (i == j)
	    {
		printf("%lf ", m_p->a[i]);
	    }
	    else if (i == j+1 || i == j-1)
	    {
		printf("%lf ", m_p->b[i]);
	    }
	    else
	    {
		printf("%lf ", 0.);
	    }
	}
	printf("\n");
    }
}

void destroy_matrix3(matrix3* m_p)
{
    if (NULL != m_p->a)
    {
	free(m_p->a);
    }
    if (NULL != m_p->b)
    {
	free(m_p->b);
    }
    if (NULL != m_p->c)
    {
	free(m_p->c);
    }
    if (NULL != m_p->u)
    {
	free(m_p->u);
    }
    m_p->a = NULL;
    m_p->b = NULL;
    m_p->c = NULL;
    m_p->u = NULL;
}

void solve(matrix3* m_p)
{
    int i;
    m_p->u[0] /= m_p->a[0];
    for (i = 1; i<m_p->n; i++)
    {
	m_p->u[i] = (m_p->u[i] - m_p->b[i]*m_p->u[i-1])/m_p->a[i];
    }
    for (i = m_p->n-2; i>=0; i--)
    {
	m_p->u[i] = m_p->u[i] - m_p->c[i]*m_p->u[i+1];
    }
}

void update_matrix3_with_field(matrix3* m_p, field* f_p, Geometry* g, int dx, double nu, Parameters* p)
{
    int i,j,k;
    double c = 1./g->ndim;
    int idx = 0;
    for (k = 1; k<g->nz-1; k++)
    {
	for (j = 1; j<g->ny-1; j++)
	{
	    for (i = 1; i<g->nx-1; i++)
	    {
		int index = INDEX_P(f_p,i,j,k);
		m_p->u[idx] =
		    c * nu * f_p->a[index - dx] +
		    (1. - 2.* nu * c) * f_p->a[index] +
		    c * nu * f_p->a[index + dx] +
		    c * g->b * source(i,j,k,g, p);
		idx++;
	    }
	}
    }
}

void extract_field_from_matrix3(matrix3* m_p, field* f_p, Geometry* g)
{
    int i,j,k;
    int idx = 0;
    for (k = 1; k<g->nz-1; k++)
    {
	for (j = 1; j<g->ny-1; j++)
	{
	    for (i = 1; i<g->nx-1; i++)
	    {
		f_p->a[INDEX_P(f_p,i,j,k)] = m_p->u[idx];
		idx++;
	    }
	}
    }
}

