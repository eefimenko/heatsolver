#include "header.h"
void residual(matrix* a, double* x, double* res, int n)
{
    int nx = a->nx;
    int ny = a->ny;
    int i,j;
    if (nx != n || ny != n+1)
    {
	printf("Matrix and vector dimension must agree nx=%d ny=%d n=%d\n", nx,ny,n);
	exit(-1);
    }
    for (i = 0; i < n; i++)
    {
	double val = 0.;
	for (j = 0; j < n; j++)
	{
	    val += a->a[IDX(a,i,j)]*x[j];
	}
	res[i] = -(a->a[IDX(a,i,n)] - val);
    }
}


void rstrct(field *uc, field *uf, int nc)
/* 
   Half-weighting restriction. nc is the coarse-grid dimension. The fine-grid solution is input in
   uf[1..2*nc-1][1..2*nc-1], the coarse-grid solution is returned in uc[1..nc][1..nc].
*/
{
    int ic,iif,jc,jf,ncc=2*nc-1;
    /* Interior points.*/
    
    for (jf=2,jc=1;jc<nc-1;jc++,jf+=2) { 
	for (iif=2,ic=1;ic<nc-1;ic++,iif+=2) {
	    uc->a[INDEX_P(uc,ic,jc,1)] = 0.5*uf->a[INDEX_P(uf,iif,jf,1)] +
		0.125*(uf->a[INDEX_P(uf,iif+1,jf,1)] + uf->a[INDEX_P(uf,iif-1,jf,1)] +
		       uf->a[INDEX_P(uf,iif,jf+1,1)] + uf->a[INDEX_P(uf,iif,jf-1,1)]);
//	    printf("%d %d %d %d\n", ic, jc, iif, jf);
	    fflush(stdout);
	}
    }
    /* Boundary points. */
    for (jc=0,ic=0;ic<nc;ic++,jc+=2)
    {
	uc->a[INDEX_P(uc,ic,0,1)] = uf->a[INDEX_P(uf,jc,0,1)];
	uc->a[INDEX_P(uc,ic,nc-1,1)] = uf->a[INDEX_P(uf,jc,ncc-1,1)];
    }
    for (jc=0,ic=0;ic<nc;ic++,jc+=2)
    {
	uc->a[INDEX_P(uc,0,ic,1)] = uf->a[INDEX_P(uf,0,jc,1)];
	uc->a[INDEX_P(uc,nc-1,ic,1)] = uf->a[INDEX_P(uf,ncc-1,jc,1)];
    }
}

void interp(field *uf, field *uc, int nf)
/*
  Coarse-to-fine prolongation by bilinear interpolation. nf is the fine-grid dimension. The coarsegrid
  solution is input as uc[1..nc][1..nc], where nc = nf/2 + 1. The fine-grid solution is
  returned in uf[1..nf][1..nf].
*/
{
    int ic,iif,jc,jf,nc;
    nc=nf/2+1;
    if (nc != uc->nx || nc != uc->ny)
    {
	printf("Wrong size for coarse frid: nc = %d ncx=%d ncy = %d\n", nc, uc->nx, uc->ny);
	exit(-1);
    }
    if (nf != uf->nx || nf != uf->ny)
    {
	printf("Wrong size for coarse frid: nf = %d nfx=%d nfy = %d\n", nf, uf->nx, uf->ny);
	exit(-1);
    }

    /* Do elements that are copies.*/
    for (jc=0;jc<nc;jc++) 
	for (ic=0;ic<nc;ic++) 
	    uf->a[INDEX_P(uf,2*ic,2*jc,1)] = uc->a[INDEX_P(uc,ic,jc,1)];
    /* Do odd-numbered columns, interpolating vertically.*/
    for (jf=0;jf<nf;jf+=2) 
	for (iif=1;iif<nf;iif+=2) 
	    uf->a[INDEX_P(uf,iif,jf,1)]=0.5*(uf->a[INDEX_P(uf,iif+1,jf,1)] + uf->a[INDEX_P(uf,iif-1,jf,1)]);
    /*Do even-numbered columns, interpolating horizontally.*/
    for (jf=1;jf<nf;jf+=2) 
	for (iif=0;iif < nf;iif++) 
	    uf->a[INDEX_P(uf,iif,jf,1)]=0.5*(uf->a[INDEX_P(uf,iif,jf+1,1)] + uf->a[INDEX_P(uf,iif,jf-1,1)]);
}

void addint(field *uf, field *uc, field *res, int nf)
/*
  Does coarse-to-fine interpolation and adds result to uf. nf is the fine-grid dimension. The
  coarse-grid solution is input as uc[1..nc][1..nc], where nc = nf/2+1. The fine-grid solution
  is returned in uf[1..nf][1..nf]. res[1..nf][1..nf] is used for temporary storage.
*/
{
    int i,j;
    interp(res,uc,nf);
    for (j=0;j<nf;j++)
	for (i=0;i<nf;i++)
	    uf->a[INDEX_P(uf,i,j,1)] += res->a[INDEX_P(res,i,j,1)];
}

void init_multigrid(Multigrid* m, field* u1_p, Geometry* g, Parameters* p)
{
    int nn,i;
    int n = g->nx;
    
    if (g->nx != g->ny)
    {
	printf("Error: grid must be square\n");
	exit(-1);
    }
    m->ng = 0; 		    
    /*** use bitshift to find the number of grid levels, stored in ng ***/
    nn=n;                   
    while (nn >>= 1) m->ng++;     
			
    /*** some simple input checks ***/
    if (n != 1+(1L << m->ng))
    {
	printf("n-1 must be a power of 2\n");
	exit(-1);
    }
	
    printf("Number of grids: %d\n", m->ng);
   	
    m->a = (matrix*)malloc(m->ng*sizeof(matrix));
    if (NULL == m->a)
    {
	print_error("Unable to allocate memory");
    }
    m->u1 = (field*)malloc(m->ng*sizeof(field));
    if (NULL == m->u1)
    {
	print_error("Unable to allocate memory");
    }
    m->u10 = (field*)malloc(m->ng*sizeof(field));
    if (NULL == m->u10)
    {
	print_error("Unable to allocate memory");
    }
    m->ures = (field*)malloc(m->ng*sizeof(field));
    if (NULL == m->ures)
    {
	print_error("Unable to allocate memory");
    }
    m->mult = (int*)malloc(m->ng*sizeof(int));
    if (NULL == m->mult)
    {
	print_error("Unable to allocate memory");
    }
    m->num = (int*)malloc(m->ng*sizeof(int));
    if (NULL == m->num)
    {
	print_error("Unable to allocate memory");
    }
    m->gridsize = (int*)malloc(m->ng*sizeof(int));
    if (NULL == m->gridsize)
    {
	print_error("Unable to allocate memory");
    }
    m->x = (double**)malloc(m->ng*sizeof(double*));
    if (NULL == m->x)
    {
	print_error("Unable to allocate memory");
    }
    m->res = (double**)malloc(m->ng*sizeof(double*));
    if (NULL == m->res)
    {
	print_error("Unable to allocate memory");
    }
    		
    /* First perform initialisation of all variables for all grids*/
    for (i=0; i<m->ng; i++)
    {
	int d = (m->ng-1)-i;
	int n1 = (n-1)/pow(2,d)-1; 
	m->mult[i] = pow(2,d); // this number shows how many times step on i-th grid greater than fine grid step
	m->num[i] = n1*n1;
	m->gridsize[i] = n1+2; // number of step in i-th grid
	init_matrix(&m->a[i], m->num[i], m->num[i]+1); //initialise matrix for i-th grid
	initialize_diffusion_mg_scheme(&m->a[i], g, m->mult[i]); // fill in Crank-Nicholson coefficients
	m->x[i] = init_vector(m->num[i]); // init vector for solution on i-th grid
	m->res[i] = init_vector(m->num[i]); //init vector for residual on i-th grid 
	init_field(&m->u1[i], n1+2, n1+2, 3); // init field on i-th grid
	init_field(&m->u10[i], n1+2, n1+2, 3); // init auxilary field on i-th grid, that stores initial field value
	init_field(&m->ures[i], n1+2, n1+2, 3); // init auxilary field on i-th grid which stores residual
    }
    copy_field(&m->u1[m->ng-1], u1_p);
}

void destroy_multigrid(Multigrid* m)
{
    int i;
    for (i=0; i<m->ng; i++)
    {
	destroy_matrix(&m->a[i]);
	free(m->x[i]);
	free(m->res[i]);
	destroy_field(&m->u1[i]);
	destroy_field(&m->u10[i]);
	destroy_field(&m->ures[i]);
    }
    free(m->a);
    free(m->u1);
    free(m->u10);
    free(m->ures);
    free(m->mult);
    free(m->num);
    free(m->gridsize);
    free(m->x);
    free(m->res);
}

int fmg(Multigrid* m, Geometry* g, Parameters* p, double eps, int nmax)
{
    int i, vcycle, k=0, n=0;
    double norm;
    field tmp;
    init_field(&tmp,m->u1[m->ng-1].nx, m->u1[m->ng-1].ny, m->u1[m->ng-1].nz);
    norm = norm_field(&m->u1[m->ng-1]);
    copy_field(&m->u10[m->ng-1], &m->u1[m->ng-1]); //save initial rhs
    update_diffusion_matrix_with_field_mg(&m->a[0], &m->u10[0], g, p, m->mult[0], 1.);
    do
    {
/* 1. First, interpolate to the coarsest grid */
	copy_field(&tmp, &m->u1[m->ng-1]);
	for (i=m->ng-1; i>0; i--)
	{
	    rstrct(&m->u10[i-1], &m->u10[i], m->gridsize[i-1]);
	}
	
	/* Solve the equation on the coarsest grid*/
	sor(&m->a[0], m->x[0], p->eps, p->maxiteration, 1.);
	read_field_from_vector(&m->u1[0],m->x[0],g);

	/* 2. Perform a number of V-cycles, startim->ng from the coarsest grids */ 
	for (vcycle=1; vcycle<m->ng; vcycle++)
	{
	    interp(&m->u1[vcycle], &m->u1[vcycle-1], m->gridsize[vcycle]);
	    read_vector_from_field(&m->u1[vcycle], m->x[vcycle], g);
	    update_diffusion_matrix_with_field_mg(&m->a[vcycle], &m->u10[vcycle], g, p, m->mult[vcycle],1.);
	    for (i = vcycle; i>k; i--)
	    {
		sor(&m->a[i], m->x[i], p->eps, NPRE, 1.); // pre smoothing
		residual(&m->a[i], m->x[i], m->res[i], m->num[i]); // calculating residual
		read_field_from_vector(&m->ures[i], m->res[i], g);
		read_field_from_vector(&m->u1[i], m->x[i], g); // saving solution
		rstrct(&m->u1[i-1], &m->ures[i], m->gridsize[i-1]); // restiction to a coarser grid
		update_diffusion_matrix_mg(&m->a[i-1], &m->u1[i-1], g, p); // update rhs
	    }
		   
	    sor(&m->a[k], m->x[k], p->eps, p->maxiteration, 1.); // solving eqaution on the coarsest grid
		   	
	    for (i = k+1; i < vcycle+1; i++)
	    {
		read_field_from_vector(&m->u1[i-1],m->x[i-1],g);
		addint(&m->u1[i], &m->u1[i-1], &m->ures[i], m->gridsize[i]); //interpolation and adding error to a finer grid
		sor(&m->a[i], m->x[i], p->eps, NPOST, 1.); //post smoothing
	    }
	    read_field_from_vector(&m->u1[vcycle],m->x[vcycle],g);
	}
	n++;
    }
    while(diff_field(&tmp, &m->u1[m->ng-1]) > norm*eps*eps && n < nmax);
    return n;
}

int vsweep(Multigrid* m, Geometry* g, Parameters* p, double eps, int nmax)
{
    int i, k=0, n=0;
    double norm;
    field tmp;
    init_field(&tmp, m->u1[m->ng-1].nx, m->u1[m->ng-1].ny, m->u1[m->ng-1].nz);
    norm = norm_field(&m->u1[m->ng-1]);
    copy_field(&m->u10[m->ng-1], &m->u1[m->ng-1]); //save initial rhs
    update_diffusion_matrix_with_field_mg(&m->a[m->ng-1], &m->u10[m->ng-1], g, p, m->mult[m->ng-1],1.); 
    do
    {
	copy_field(&tmp, &m->u1[m->ng-1]);
	//update rhs on the finest grid
	for (i = m->ng-1; i>k; i--)
	{
	    sor(&m->a[i], m->x[i], p->eps, NPRE, 1.); // perform pre smoothing, few GS iterations
	    residual(&m->a[i], m->x[i], m->res[i], m->num[i]); // calculate residual b - Ax
	    read_field_from_vector(&m->ures[i], m->res[i], g); // read residual into matrix
	    read_field_from_vector(&m->u1[i], m->x[i], g); // save field into matrix
	    rstrct(&m->u1[i-1], &m->ures[i], m->gridsize[i-1]); // restrict difference to coarser grid
	    update_diffusion_matrix_mg(&m->a[i-1], &m->u10[i-1], g, p); //update rhs 
	}
	// solve on the coarsest grid, few GS iterations
	sor(&m->a[k], m->x[k], p->eps, p->maxiteration, 1.);
		   		   
	for (i = k+1; i < m->ng; i++)
	{
	    read_field_from_vector(&m->u1[i-1],m->x[i-1],g);
	    // interpolate and add sifference to solution on the upper layer
	    addint(&m->u1[i],&m->u1[i-1],&m->ures[i],m->gridsize[i]);
	    sor(&m->a[i], m->x[i], p->eps, NPOST, 1.); //post smoothing
	}
	read_field_from_vector(&m->u1[m->ng-1],m->x[m->ng-1],g);
	n++;
    }
    while(diff_field(&tmp, &m->u1[m->ng-1]) > norm*eps*eps && n < nmax);
    return n;
}
/* Multigrid preconditioner */
void mg_pre(Preconditioner* pre, double* b, double* x, Geometry* g, Parameters* p)
{
    Multigrid* m = &pre->m;
    int i, k=0,vcycle;
    //update rhs on the finest grid
    read_field_from_vector(&m->u1[m->ng-1], b, g);
    copy_field(&m->u10[m->ng-1], &m->u1[m->ng-1]);
    
    for (i=m->ng-1; i>0; i--)
    {
	rstrct(&m->u10[i-1], &m->u10[i], m->gridsize[i-1]);
    }
    update_diffusion_matrix_mg(&m->a[0], &m->u10[0], g, p);
    /* Solve the equation on the coarsest grid*/
    sor_n(&m->a[0], m->x[0], p->eps, p->maxiteration, 1.);
    read_field_from_vector(&m->u1[0],m->x[0],g);

    /* 2. Perform a number of V-cycles, starting from the coarsest grids */ 
    for (vcycle=1; vcycle<m->ng; vcycle++)
    {
	interp(&m->u1[vcycle], &m->u1[vcycle-1], m->gridsize[vcycle]);
	read_vector_from_field(&m->u1[vcycle], m->x[vcycle], g);
	update_diffusion_matrix_mg(&m->a[vcycle], &m->u10[vcycle], g, p);
	for (i = vcycle; i>k; i--)
	{
	    sor(&m->a[i], m->x[i], p->eps, 3, 1.); // pre smoothing
	    residual(&m->a[i], m->x[i], m->res[i], m->num[i]); // calculating residual
	    read_field_from_vector(&m->ures[i], m->res[i], g);
	    read_field_from_vector(&m->u1[i], m->x[i], g); // saving solution
	    rstrct(&m->u1[i-1], &m->ures[i], m->gridsize[i-1]); // restiction to a coarser grid
	    update_diffusion_matrix_mg(&m->a[i-1], &m->u1[i-1], g, p); // update rhs
	}
		   
	sor(&m->a[k], m->x[k], p->eps, p->maxiteration, 1.); // solving eqaution on the coarsest grid
		   	
	for (i = k+1; i < vcycle+1; i++)
	{
	    read_field_from_vector(&m->u1[i-1],m->x[i-1],g);
	    addint(&m->u1[i], &m->u1[i-1], &m->ures[i], m->gridsize[i]); //interpolation and adding error to a finer grid
	    sor(&m->a[i], m->x[i], p->eps, 3, 1.); //post smoothing
	}
    }
    copy_vector(x, m->x[m->ng-1], m->num[m->ng-1]);
}


