#include "header.h"

Parameters *init_parameters(void)
{
    Parameters *p = malloc(sizeof(Parameters));
    p->lx = 1.;
    p->ly = 1.;
    p->lz = 1.;
    p->nx = 20;
    p->ny = 20;
    p->nz = 20;
    p->dt = 0.01;
    p->d = 0.0001;
    p->eps = 1e-6;
    p->x0 = 0.5;
    p->y0 = 0.5;
    p->z0 = 0.5;
    p->a = 0.25;
    p->b = 0.25;
    p->c = 0.25;
    p->t0 = 0.;
    p->t1 = 1.;
    p->tr = 1.;
    p->num = 100;
    p->save = 100;
    p->prof = 10;
    p->bc = DIRICHLET;
    p->bcx = DIRICHLET;
    p->bcy = DIRICHLET;
    p->bcz = DIRICHLET;
    p->solver = FTCS;
    p->equation = DIFFUSION;
    p->num_10 = p->num/10;
    if (p->num_10 == 0)
    {
	p->num_10 = 1;
    }
    return p;
}

Geometry *init_geometry(Parameters *p)
{
    Geometry *g = malloc(sizeof(Geometry));
    g->nx = p->nx;
    g->ny = p->ny;
    g->nz = p->nz;
    g->dx = p->lx/(p->nx-1);
    g->dy = p->ly/(p->ny-1);
    g->dz = p->lz/(p->nz-1);
    g->bc = p->bc;
    g->bcx = p->bcx;
    g->bcy = p->bcy;
    g->bcz = p->bcz;
    
    g->ax = p->d*p->dt/(g->dx*g->dx);
    g->ay = p->d*p->dt/(g->dy*g->dy);
    g->az = p->d*p->dt/(g->dz*g->dz);
    g->bx = p->vx*p->dt/g->dx;
    g->by = p->vy*p->dt/g->dy;
    g->bz = p->vz*p->dt/g->dz;
    g->b = p->dt;
    printf("%lf %lf %lf\n", g->bx,  g->by, g->bz);
    g->ndim = 3;
    if (g->ny == 3 && g->nz == 3) 
    {
	g->ndim = 1.;
	g->ay = 0.;
	g->az = 0.;
	g->by = 0.;
	g->bz = 0.;
	g->bcy = PERIODIC;
	g->bcz = PERIODIC;
    }
    else if (g->nz == 3)
    {
	g->ndim = 2.;
	g->az = 0.;
	g->bz = 0.;
	g->bcz = PERIODIC;
    }
    
    if (p->solver == FTCS)
    {
	if (p->equation == DIFFUSION)
	{
	    int stable = (g->ax + g->ay + g->az) < 0.5;
	
	    if (stable > 0)
	    {
		printf("Explicit scheme is stable\n");
	    }
	    else
	    {
		printf("Scheme is unstable, please adjust the parameters\n");
		printf("nu_x = %lf nu_y = %lf nu_z = %lf nu_x + nu_y + nu_z = %lf > 0.5\n",
		       g->ax, g->ay, g->az, g->ax + g->ay + g->az);
		exit(-1);
	    }
	}
	else if (p->equation == ADVECTION)
	{
	    int stable1 = (g->ax + g->ay + g->az) < 0.5;
	    if (stable1 > 0)
	    {
		printf("Condition 1 is fullfilled\n");
	    }
	    else
	    {
		printf("Scheme is unstable, please adjust the parameters\n");
		printf("%lf > 0.5\n", g->ax + g->ay + g->az);
	    }
	    int stable2 = fabs(p->vx*p->dt/g->dx) + fabs(p->vy*p->dt/g->dy) + fabs(p->vz*p->dt/g->dz)< 0.5;
	    if (stable2 > 0)
	    {
		printf("Condition 2 is fullfilled\n");
	    }
	    else
	    {
		printf("Scheme is unstable, please adjust the parameters\n");
		printf("%lf > 0.5\n",
		       fabs(p->vx*p->dt/g->dx) + fabs(p->vy*p->dt/g->dy) + fabs(p->vz*p->dt/g->dz));
	    }
	    int stable3 = (p->vx*p->vx + p->vy*p->vy + p->vz*p->vz)*p->dt/p->d < 3.;
	    if (stable3 > 0)
	    {
		printf("Condition 3 is fullfilled\n");
	    }
	    else
	    {
		printf("Scheme is unstable, please adjust the parameters\n");
		printf("%lf > 3.\n",
		       (p->vx*p->vx + p->vy*p->vy + p->vz*p->vz)*p->dt/p->d);
	    }
	}
    }
    else
    {
	printf("Scheme is unconditionally stable\n");
    }
    return g;
}


