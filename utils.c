#include "header.h"

double timer(void)
{
  struct timeval time;
  gettimeofday(&time, 0);
  return time.tv_sec + time.tv_usec/1000000.0;
}

void print_value(const char* name, double time)
{
    FILE* out;
    if (NULL == (out = fopen(name, "w")))
    {
	fprintf(stderr, "Unable open file %s for writing\n", name);
	exit(-1);
    }
    fprintf(out, "%lf\n", time);
    fclose(out);
}

int copy_field(field* dst, field* src)
{
    if (dst->nx != src->nx || dst->ny != src->ny)
    {
	printf("Error, vector sizes must be the same\n");
	exit(-1);
    }
    memcpy(dst->a, src->a, sizeof(double)*src->nx*src->ny*src->nz);
    return 0;
}

int swap_field(field* dst, field* src)
{
    double* tmp;
    if (dst->nx != src->nx || dst->ny != src->ny)
    {
	printf("Error, vector sizes must be the same\n");
	exit(-1);
    }
    tmp = dst->a;
    dst->a = src->a;
    src->a = tmp;
    return 0;
}

int copy_vector(double* dst, double* src, int n)
{
    memcpy(dst, src, sizeof(double)*n);
    return 0;
}

int init_field(field* f_p, int nx, int ny, int nz)
{
    int bytes = sizeof(double)*nx*nz*ny;
    f_p->nx = nx;
    f_p->ny = ny;
    f_p->nz = nz;
    f_p->nxy = nx*ny;

    if (0 == bytes)
    {
        f_p->a = NULL;
        return 0;
    }

    f_p->a = (double*)malloc(bytes);

    if (NULL == f_p->a)
    {
        fprintf(stderr, "Not enough memory %d bytes: %d %d %d\n", bytes, nx, ny, nz);
	fflush(stderr);
        exit(-1);
    }
    memset(f_p->a, 0, bytes);

    return 0;
}



void destroy_field(field* f_p)
{
    if (0 == f_p->nx ||	0 == f_p->ny ||	0 == f_p->nz ||	NULL == f_p->a)
    {
        return;
    }

    free(f_p->a);
    f_p->a = NULL;
}



double uran(double a)
{
    return rand() / (RAND_MAX + 1.0) * a;
}

void set_initial_distribution(field* f_p, Geometry* g, Parameters* p)
{
    int i,j,k;
    for(i = 0; i<f_p->nx; i++)
    {
	for(j = 0; j<f_p->ny; j++)
	{
	    for(k = 0; k<f_p->nz; k++)
	    {
		double x = i*g->dx;
		double y = j*g->dy;
		double z = k*g->dz;
		f_p->a[INDEX_P(f_p,i,j,k)] = p->t0 + p->t1 *
		    exp(-(x - p->x0)*(x - p->x0)/(p->a*p->a)) *
		    exp(-(y - p->y0)*(y - p->y0)/(p->b*p->b)) *
		    exp(-(z - p->z0)*(z - p->z0)/(p->c*p->c)) +
		    uran(p->tr);
	    }
	}	
    }
    apply_constant_bc(f_p, g, p);
    apply_periodic_bc(f_p, g);
}

void set_initial_distribution_mg(field* f_p, Geometry* g, Parameters* p, double m)
{
    int i,j,k;
    for(i = 0; i<f_p->nx; i++)
    {
	for(j = 0; j<f_p->ny; j++)
	{
	    for(k = 0; k<f_p->nz; k++)
	    {
		double x = i*g->dx*m;
		double y = j*g->dy*m;
		double z = k*g->dz*m;
		f_p->a[INDEX_P(f_p,i,j,k)] = p->t0 + p->t1 *
		    exp(-(x - p->x0)*(x - p->x0)/(p->a*p->a)) *
		    exp(-(y - p->y0)*(y - p->y0)/(p->b*p->b)) *
		    exp(-(z - p->z0)*(z - p->z0)/(p->c*p->c)) +
		    uran(p->tr);
	    }
	}	
    }
    apply_constant_bc(f_p, g, p);
    apply_periodic_bc(f_p, g);
}
double norm_field(field* f1_p)
{
    int i,j,k;
    double norm = 0.;
    for(i = 0; i<f1_p->nx; i++)
    {
	for(j = 0; j<f1_p->ny; j++)
	{
	    for(k = 0; k<f1_p->nz; k++)
	    {
		int idx = INDEX_P(f1_p,i,j,k);
		double f1 = f1_p->a[idx];
		norm += f1*f1;
	    }
	}	
    }
    return norm;
}
double diff_field(field* f1_p, field* f2_p)
{
    int i,j,k;
    double eps = 0.;
    for(i = 0; i<f1_p->nx; i++)
    {
	for(j = 0; j<f1_p->ny; j++)
	{
	    for(k = 0; k<f1_p->nz; k++)
	    {
		int idx = INDEX_P(f1_p,i,j,k);
		double f1 = f1_p->a[idx];
		double f2 = f2_p->a[idx];
		eps += (f1-f2)*(f1-f2);
	    }
	}	
    }
    return eps;
}

void save_field(field* f_p, int t, Geometry* g)
{
    FILE* out;
    char name[64];
    int i,j,k;
    int i0 = f_p->nx/2;
    int j0 = f_p->ny/2;
    int k0 = f_p->nz/2;
    
    if (g->ny > 3 && g->nz > 3)
    {
	sprintf(name, "field_x_%06d.dat", t);
	if (NULL == (out = fopen(name,"w")))
	{
	    fprintf(stderr,"Unable to open file %s for writing\n", name);
	    exit(-1);
	}
	fprintf(out, "# %d %d %le %le\n", f_p->ny, f_p->nz, g->dy, g->dz);
	for(j = 0; j<f_p->ny; j++)
	{
	    for(k = 0; k<f_p->nz; k++)
	    {
		fprintf(out, "%d %d %le\n", j, k, f_p->a[INDEX_P(f_p,i0,j,k)]);
	    }
	}
	fclose(out);
    }
    
    if (g->nx > 3 && g->nz > 3)
    {
	sprintf(name, "field_y_%06d.dat", t);
	if (NULL == (out = fopen(name,"w")))
	{
	    fprintf(stderr,"Unable to open file %s for writing\n", name);
	    exit(-1);
	}
	fprintf(out, "# %d %d %le %le\n", f_p->nx, f_p->nz, g->dx, g->dz);
	for(i = 0; i<f_p->nx; i++)
	{
	    for(k = 0; k<f_p->nz; k++)
	    {
		fprintf(out, "%d %d %le\n", i, k, f_p->a[INDEX_P(f_p,i,j0,k)]);
	    }
	}
	fclose(out);
    }
    
    if (g->nx > 3 && g->ny > 3)
    {
	sprintf(name, "field_z_%06d.dat", t);
	if (NULL == (out = fopen(name,"w")))
	{
	    fprintf(stderr,"Unable to open file %s for writing\n", name);
	    exit(-1);
	}
	fprintf(out, "# %d %d %le %le\n", f_p->nx, f_p->ny, g->dx, g->dy);
	for(i = 0; i<f_p->nx; i++)
	{
	    for(j = 0; j<f_p->ny; j++)
	    {
		fprintf(out, "%d %d %le\n", i, j, f_p->a[INDEX_P(f_p,i,j,k0)]);
	    }
	}
	fclose(out);
    }
}

void max_avg_value(field* f_p, double* max, double*avg)
{
    double tmp = -1e99;
    double sum = 0;
    int i,j,k;
    for(i = 0; i<f_p->nx; i++)
    {
	for(j = 0; j<f_p->ny; j++)
	{
	    for(k = 0; k<f_p->nz; k++)
	    {
		if (f_p->a[INDEX_P(f_p,i,j,k)] > tmp)
		{
		    tmp = f_p->a[INDEX_P(f_p,i,j,k)];
		}
		sum += f_p->a[INDEX_P(f_p,i,j,k)];
	    }
	}	
    }
    *max = tmp;
    *avg = sum/(f_p->nx*f_p->ny*f_p->nz);
}

void print_field(field* f_p)
{
    int i,j,k;
    for(k = 1; k<f_p->nz-1; k++)
    {
	for(j = 0; j<f_p->ny; j++)
	{
	    for(i = 0; i<f_p->nx; i++)
	    {
		printf("%lf ", f_p->a[INDEX_P(f_p,i,j,k)]);
	    }
	    printf("\n");
	}
	printf("\n");	
    }
}

void save_profile(field* f_p, int t, Parameters* p, Geometry* g)
{
    FILE* out;
    int i,j,k;
    int i0 = f_p->nx/2;
    int j0 = f_p->ny/2;
    int k0 = f_p->nz/2;
//    printf("i0=%d j0=%d k0=%d\n", i0,j0,k0);
    if (t == 0)
    {
	if (g->nx > 3)
	{
	    if (NULL == (out = fopen("profilex.dat","w")))
	    {
		fprintf(stderr,"Unable to open file profilex.dat for writing\n");
		exit(-1);
	    }
	    fprintf(out, "# %d %d %le %le\n", f_p->nx, p->num/p->prof + 1, g->dx, p->dt);
	    for(i = 0; i<f_p->nx; i++)
	    {
		fprintf(out, "%d %d %le\n", i, t/p->prof, f_p->a[INDEX_P(f_p,i,j0,k0)]);
	    }
	    fclose(out);
	}
	if (g->ny > 3)
	{
	    if (NULL == (out = fopen("profiley.dat","w")))
	    {
		fprintf(stderr,"Unable to open file profiley.dat for writing\n");
		exit(-1);
	    }
	    fprintf(out, "# %d %d %le %le\n", f_p->ny, p->num/p->prof + 1, g->dy, p->dt);
	    for(j = 0; j<f_p->ny; j++)
	    {
		fprintf(out, "%d %d %le\n", j, t/p->prof, f_p->a[INDEX_P(f_p,i0,j,k0)]);
	    }
	    fclose(out);
	}
	if (g->nz > 3)
	{
	    if (NULL == (out = fopen("profilez.dat","w")))
	    {
		fprintf(stderr,"Unable to open file profilez.dat for writing\n");
		exit(-1);
	    }
	    fprintf(out, "# %d %d %le %le\n", f_p->nz, p->num/p->prof + 1, g->dz, p->dt);
	    for(k = 0; k<f_p->nz; k++)
	    {
		fprintf(out, "%d %d %le\n", k, t/p->prof, f_p->a[INDEX_P(f_p,i0,j0,k)]);
	    }
	    fclose(out);
	}
    }
    else
    {
	if (g->nx > 3)
	{
	    if (NULL == (out = fopen("profilex.dat","a")))
	    {
		fprintf(stderr,"Unable to open file profilex.dat for writing\n");
		exit(-1);
	    }
	    for(i = 0; i<f_p->nx; i++)
	    {
		fprintf(out, "%d %d %le\n", i, t/p->prof, f_p->a[INDEX_P(f_p,i,j0,k0)]);
	    }
	    fclose(out);
	}
	if (g->ny > 3)
	{
	    if (NULL == (out = fopen("profiley.dat","a")))
	    {
		fprintf(stderr,"Unable to open file profiley.dat for writing\n");
		exit(-1);
	    }
	    for(j = 0; j<f_p->ny; j++)
	    {
		fprintf(out, "%d %d %le\n", j, t/p->prof, f_p->a[INDEX_P(f_p,i0,j,k0)]);
	    }
	    fclose(out);
	}
	if (g->nz > 3)
	{
	    if (NULL == (out = fopen("profilez.dat","a")))
	    {
		fprintf(stderr,"Unable to open file profilez.dat for writing\n");
		exit(-1);
	    }
	    
	    for(k = 0; k<f_p->nz; k++)
	    {
		fprintf(out, "%d %d %le\n", k, t/p->prof, f_p->a[INDEX_P(f_p,i0,j0,k)]);
	    }
	    fclose(out);
	}
    }
}

void save_data(field* f_p, int t, Parameters* p, Geometry* g, double t1)
{
    double max, avg, t2;
    
    if (p->save > 0 && t > 0 && t % p->save == 0)
    {
	save_field(f_p, t, g);
    }
    if (t % p->num_10 == 0)
    {
	max_avg_value(f_p, &max, &avg);
	t2 = timer();
	printf("%6d iteration time %lf s: average temperature %lf max temperature %lf\n", t, t2-t1, avg, max);
    }
    if (t % p->prof == 0)
    {
	save_profile(f_p, t, p, g);
    }
}


