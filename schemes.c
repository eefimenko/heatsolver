#include "header.h"

void update_diffusion_ftcs(field* f_p, field* fnew_p, Geometry* g, Parameters* p)
{
    int i,j,k;
    int dx = DX(f_p);
    int dy = DY(f_p);
    int dz = DZ(f_p);

    for(i = 1; i<f_p->nx-1; i++)
    {
	for(j = 1; j<f_p->ny-1; j++)
	{
	    for(k = 1; k<f_p->nz-1; k++)
	    {
		int idx = INDEX_P(f_p,i,j,k);
		double f = f_p->a[idx];
		fnew_p->a[idx] = f +
		    g->ax*(f_p->a[idx - dx] + f_p->a[idx + dx] - 2.*f) +
		    g->ay*(f_p->a[idx - dy] + f_p->a[idx + dy] - 2.*f) +
		    g->az*(f_p->a[idx - dz] + f_p->a[idx + dz] - 2.*f)
		    + g->b*source(i,j,k,g, p);
	    }
	}	
    }
}

void update_advection_ftcs(field* f_p, field* fnew_p, Geometry* g, Parameters* p)
{
    int i,j,k;
    int dx = DX(f_p);
    int dy = DY(f_p);
    int dz = DZ(f_p);
  
    for(i = 1; i<f_p->nx-1; i++)
    {
	for(j = 1; j<f_p->ny-1; j++)
	{
	    for(k = 1; k<f_p->nz-1; k++)
	    {
		int idx = INDEX_P(f_p,i,j,k);
		double f = f_p->a[idx];
		fnew_p->a[idx] = f -
		    0.5*g->bx*(f_p->a[idx - dx] - f_p->a[idx + dx]) -
		    0.5*g->by*(f_p->a[idx - dy] - f_p->a[idx + dy]) -
		    0.5*g->bz*(f_p->a[idx - dz] - f_p->a[idx + dz]) +
		    g->ax*(f_p->a[idx - dx] + f_p->a[idx + dx] - 2.*f) +
		    g->ay*(f_p->a[idx - dy] + f_p->a[idx + dy] - 2.*f) +
		    g->az*(f_p->a[idx - dz] + f_p->a[idx + dz] - 2.*f)
		    + g->b*source(i,j,k,g, p);
	    }
	}	
    }
}

void initialize_adi_scheme(matrix3* m_p, double nu, Geometry* g)
{
    int i;
    double c = 1./g->ndim;
    for (i = 0; i<m_p->n; i++)
    {
	m_p->a[i] = 1 + 2. * nu * c;
	m_p->b[i] = -c * nu;
	m_p->c[i] = -c * nu;
    }

    m_p->c[0] /= m_p->a[0];
    
    for (i = 1; i<m_p->n; i++)
    {
	m_p->a[i] -= m_p->b[i]*m_p->c[i-1];
	m_p->c[i] /= m_p->a[i];
    }
}

void initialize_diffusion_crni_scheme(matrix* m_p, Geometry* g)
{
    int i,j;
    int nx = g->nx-2;
    int ny = g->ny-2;
    
    for (i=0; i<m_p->nx; i++)
    {
	for (j=0; j<m_p->ny-1; j++)
	{
	    int idx = IDX(m_p,i,j);
	    if (i == j)
	    {
		m_p->a[idx] = 1. + g->ax + g->ay + g->az;
	    }
	    else if (i == j+1 || j == i+1)
	    {
		m_p->a[idx] = -0.5*g->ax;
	    }
	    else if (i == j + nx || i == j - nx)
	    {
		m_p->a[idx] = -0.5*g->ay;
	    }
	    else if (i == j + nx*ny || i == j - nx*ny)
	    {
		m_p->a[idx] = -0.5*g->az;
	    }
	    else 
	    {
		m_p->a[idx] = 0.;
	    }
	}  
    }
}

void initialize_diffusion_mg_scheme(matrix* m_p, Geometry* g, int mult)
{
    int i,j;
    int n = (g->nx-1)/mult - 1;
    
    double m2 = 1./(mult*mult);
    
    for (i=0; i<m_p->nx; i++)
    {
	for (j=0; j<m_p->ny; j++)
	{
	    int idx = IDX(m_p,i,j);
	    if (i == j)
	    {
		m_p->a[idx] = 1. + g->ax*m2 + g->ay*m2;
	    }
	    else if (i == j+1 || j == i+1)
	    {
		m_p->a[idx] = -0.5*g->ax*m2;
	    }
	    else if (i == j + n || i == j - n)
	    {
		m_p->a[idx] = -0.5*g->ay*m2;
	    }
	    else 
	    {
		m_p->a[idx] = 0.;
	    }
	}  
    }
}

void initialize_advection_crni_scheme(matrix* m_p, Geometry* g)
{
    int i,j;
    int nx = g->nx-2;
    int ny = g->ny-2;
    
    for (i=0; i<m_p->nx; i++)
    {
	for (j=0; j<m_p->ny-1; j++)
	{
	    int idx = IDX(m_p,i,j);
	    
	    if (i == j)
	    {
		m_p->a[idx] = 1. + g->ax + g->ay + g->az;
	    }
	    else if (i == j+1 || j == i+1)
	    {
		m_p->a[idx] = -0.5*g->ax;
		if (j == i+1)
		{
		    m_p->a[idx] -= 0.25*g->bx; 
		}
		if (i == j+1)
		{
		    m_p->a[idx] += 0.25*g->bx; 
		}
	    }
	    else if (i == j + ny || i == j - ny)
	    {
		m_p->a[idx] = -0.5*g->ay;
		if (j == i+ny)
		{
		    m_p->a[idx] -= 0.25*g->by; 
		}
		if (i == j+ny)
		{
		    m_p->a[idx] += 0.25*g->by; 
		}
	    }
	    else if (i == j + nx*ny || i == j - nx*ny)
	    {
		m_p->a[idx] = -0.5*g->az;
		if (j == i+nx*ny)
		{
		    m_p->a[idx] -= 0.25*g->bz; 
		}
		if (i == j+nx*ny)
		{
		    m_p->a[idx] += 0.25*g->bz; 
		}
	    }
	    else 
	    {
		m_p->a[idx] = 0.;
	    }
	}  
    }
}

double source(int i, int j, int k, Geometry* g, Parameters* p)
{
    double x = i*g->dx;
    double y = j*g->dy;
    double z = k*g->dz;

    return p->samp*exp(-(x - p->sx0)*(x - p->sx0)/(p->sa*p->sa)) *
	exp(-(y - p->sy0)*(y - p->sy0)/(p->sb*p->sb)) *
	exp(-(z - p->sz0)*(z - p->sz0)/(p->sc*p->sc));
}

double source_mg(int i, int j, int k, Geometry* g, Parameters* p, int mult)
{
    double x = i*g->dx*mult;
    double y = j*g->dy*mult;
    double z = k*g->dz;

    return p->samp*exp(-(x - p->sx0)*(x - p->sx0)/(p->sa*p->sa)) *
	exp(-(y - p->sy0)*(y - p->sy0)/(p->sb*p->sb)) *
	exp(-(z - p->sz0)*(z - p->sz0)/(p->sc*p->sc));
}

