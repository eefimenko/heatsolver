#!/usr/bin/python
import gzip
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib as mp
import numpy as np
import math
import sys
import os

def read_config(path):
    f = open(path+'/config.log')
    config = {}
    for line in f:
        array = line.split()
        if array[0] != '#':
            name = array[1].strip('"')
            value = array[2].strip('"')
            config[name.strip()] = value
    return config

def read_file(filename, a=1.):
    f = open(filename, 'r')
    line0 = f.readline().split(' ')
    
    nx = int(line0[1])
    ny = int(line0[2])
    dx = float(line0[3])
    dy = float(line0[4])

    fx = [[0. for x in range(nx)] for x in range(ny)]
       
    for line in f:
        array = line.split(' ')
        x = int(array[1])
        y = int(array[0])
       
        if x >= 0 and y >= 0:
            fx[x][y] = float(array[2])/a
           
    f.close()
    return nx,ny,dx,dy,fx

           
def main():
    path = sys.argv[1]
    iteration = int(sys.argv[2])
    nmax = 0
    i=0
    profx = os.path.exists(path + '/field_x_%06d.dat'%iteration)
    profy = os.path.exists(path + '/field_y_%06d.dat'%iteration)
    profz = os.path.exists(path + '/field_z_%06d.dat'%iteration)
    if profx:
        nmax += 1
    if profy:
        nmax += 1
    if profz:
        nmax += 1
        
    if nmax ==0:
        print 'No files, exiting'
        exit(-1)
        
    fig = plt.figure(figsize=(5*nmax,5))
    if profx:
        i += 1
        nx, ny, dx, dy, f = read_file(path + '/field_x_%06d.dat'%iteration)
        axa = fig.add_subplot(1,nmax,i)
        surf = axa.imshow(f, extent=[0, nx*dx, 0, ny*dy], origin='lower')
        plt.colorbar(surf, orientation  = 'vertical')
        axa.set_xlabel('y')
        axa.set_ylabel('z')
    if profy:
        i += 1
        nx, ny, dx, dy, f = read_file(path + '/field_y_%06d.dat'%iteration)
        axa = fig.add_subplot(1,nmax,i)
        surf = axa.imshow(f, extent=[0, nx*dx, 0, ny*dy], origin='lower')
        plt.colorbar(surf, orientation  = 'vertical')
        axa.set_xlabel('x')
        axa.set_ylabel('z')
    if profz:
        i += 1
        nx, ny, dx, dy, f = read_file(path + '/field_z_%06d.dat'%iteration)
        axa = fig.add_subplot(1,nmax,i)
        surf = axa.imshow(f, extent=[0, nx*dx, 0, ny*dy], origin='lower')
        plt.colorbar(surf, orientation  = 'vertical')
        axa.set_xlabel('x')
        axa.set_ylabel('y')

    plt.show() 
 
if __name__ == '__main__':
    main()
