#include "header.h"

int cg(matrix *A, double *x, double *b, double* p, double* r, double* ap, double rtol, int maxiter)
{
    int n = A->nx;
       
    const int nbytes = n * sizeof(double);

    double bnorm2;              
    double rr, rr_old;           
    double alpha, beta;
 
    int i = 0,j = 0;                

    /* First extract right-hand side from the matrix */
    for (j = 0; j<n; j++)
    {
	b[j] = A->a[IDX(A,j,n)];
    }
    
    bnorm2    = ddot(b, b, n);
    memset(x, 0, nbytes);	//initial guess
    memcpy(p, b, nbytes);
    memcpy(r, b, nbytes);
    
    rr = ddot(r, r, n);    
    
    for (i = 0; i < maxiter ; ++i) {
        matvec(ap, A, p, n);	//ap = A*p
        alpha = rr/ddot(p, ap, n);	//alpha = rkT*rk/pkT*A*pk
	rr_old = rr;
	update_vectors(x, alpha, p, n);	 //xk+1 = xk + ak*pk
        update_vectors(r, -alpha, ap, n); //rk+1 = rk - ak*A*pk
  
	rr = ddot(r,r,n);
	if(rr <= bnorm2 * rtol * rtol)
	    break;
	beta = rr/rr_old;
        sum_vectors(p, beta, 1., p, r, n); //pk+1 = rk+1+bk*pk
    }
   
    return i;
}

int pcg(matrix *A, double *x, double* b, double* r, double* z, double* d, double* ap, double rtol, int maxiter, double w, Preconditioner *pre, Geometry* g, Parameters* p)
{
    int n = A->nx;
       
    const int nbytes = n * sizeof(double);
    int i = 0,j = 0;    
    double bnorm2;              
    double rr, rz, rz_old;           
    double alpha, beta;

    /* First extract right-hand side from the matrix */
    for (j = 0; j<n; j++)
    {
	b[j] = A->a[IDX(A,j,n)];
    }

    bnorm2    = ddot(b, b, n);
    memset(x, 0, nbytes);	//initial guess
    memcpy(r, b, nbytes);
    
    if (p->preconditioner == MG_PRE)
    {
	mg_pre(pre, r, z, g, p);
    }
    else if (p->preconditioner == SSOR_PRE)
    {
	ssor_pre(pre, A, r, z);
    }
    else if (p->preconditioner == JACOBI_PRE)
    {
	jacobi_pre(pre, A, r, z);
    }
    else
    {
	print_error("Unknown preconditioner");
    }
    
    memcpy(d, z, nbytes);

    rr = ddot(r, r, n);    
    rz = ddot(r, z, n);
    
    for (i = 0; i < maxiter ; ++i) {
        matvec(ap, A, d, n);	//=A*pk
	alpha = rz/ddot(d, ap, n); //ak = rkT*zk/pkT*A*pk
	rz_old = rz;
	update_vectors(x, alpha, d, n);	//xk+1 = xk + ak*pk
        update_vectors(r, -alpha, ap, n); //rk+1 = rk - ak*A*pk
	
	if (p->preconditioner == MG_PRE)
	{
	    mg_pre(pre, r, z, g, p);
	}
	else if (p->preconditioner == SSOR_PRE)
	{
	    ssor_pre(pre, A, r, z);
	}
	else if (p->preconditioner == JACOBI_PRE)
	{
	    jacobi_pre(pre, A, r, z);
	}
	else
	{
	    print_error("Unknown preconditioner");
	}

	rr = ddot(r,r,n);

	rz = ddot(r, z, n);
	if(rr <= bnorm2 * rtol * rtol)
	    break;
	beta = rz/rz_old;
        sum_vectors(d, beta, 1., d, z, n);	//pk+1 = zk+1+bk*pk
    }
    return i;
}

void init_preconditioner(Preconditioner* pre, field* u1_p, matrix* m, double w, Geometry* g, Parameters* p)
{
    pre->n = m->nx;
    pre->w = w;
    pre->w2 = (2-w)/w;
    if (p->preconditioner == JACOBI_PRE || p->preconditioner == SSOR_PRE)
    {
	int i;
	pre->pre = init_vector(pre->n);
	if (NULL == pre->pre)
	{
	    print_error("Unable to allocate memory in init_preconditioner");
	}
	for (i = 0; i < pre->n; i++)
	{
	    pre->pre[i] = 1./m->a[IDX(m,i,i)]*w;
	}
    }
    else if (p->preconditioner == MG_PRE)
    {
	init_multigrid(&pre->m, u1_p, g, p);
    }
}

void destroy_preconditioner(Preconditioner* pre, Parameters* p)
{
    if (p->preconditioner == JACOBI_PRE || p->preconditioner == SSOR_PRE)
    {
	free(pre->pre);
    }
    else if (p->preconditioner == MG_PRE)
    {
	destroy_multigrid(&pre->m);
    }
}
/* Simple Jacobi preconditioner */
void jacobi_pre(Preconditioner* pre, matrix* m, double* b, double* x)
{
    int i;

    for (i = 0; i < pre->n; i++)
    {
	x[i] = b[i]*pre->pre[i];
    }
}

/* http://www.netlib.org/linalg/html_templates/node61.html */
void ssor_pre(Preconditioner* pre, matrix* m, double* b, double* x)
{
    double z[pre->n];
    int i,j;
   
    for (i = 0; i < pre->n; i++)
    {
	double val = b[i];
	for (j=0; j<i; j++)
	{
	    val -= m->a[IDX(m,i,j)]*z[j];
	}
	z[i] = val*pre->pre[i];
    }
    for (i = pre->n-1; i>=0; i--)
    {
	double val = z[i];
	for (j=i+1; j<pre->n; j++)
	{
	    val -= m->a[IDX(m,i,j)]*x[j]*pre->pre[i];
	}
	x[i] = val*pre->w2;
    }

}



