#!/usr/bin/python
import matplotlib.pyplot as plt

def read_file(file):
    f = open(file)
    n = []
    r = []
    for line in f:
#        print line
        tmp = line.split()
        n.append(int(tmp[0]))
        r.append(float(tmp[1]))
    return n,r

def main():
   
    fig = plt.figure(figsize=(12,6))
    ax2 = fig.add_subplot(1,2,2)
    ax = fig.add_subplot(1,2,1)
    
    n1, r1 = read_file('sor.dat')
    n2, r2 = read_file('cg.dat')
    n3, r3 = read_file('pcg_ssor.dat')
    n5, r5 = read_file('pcg_mg.dat')
    n4, r4 = read_file('mg.dat')
    nn1, rn1 = read_file('sorn.dat')
    nn2, rn2 = read_file('cgn.dat')
    nn3, rn3 = read_file('pcg_ssorn.dat')
    nn5, rn5 = read_file('pcg_mgn.dat')
    nn4, rn4 = read_file('mgn.dat')
    
    
    ax.plot(n1, r1, 'k', label='SOR')
    ax.plot(n2, r2, 'g', label='CG')
    ax.plot(n3, r3, 'b', label='PCG(SSOR)')
    ax.plot(n4, r4, 'r', label='MG')
    ax.plot(n5, r5, 'm', label='PCG(MG)')
    ax.set_xscale('log')
#    ax.set_yscale('log')
    ax.set_ylabel('time,s')
    ax.set_xlim([n1[0], n1[-1]])
    ax.set_xticks(n1)
    ax.set_xticklabels(n1)
    ax.set_xlabel('number of steps')
    ax.set_title('Time of execution of one iteration')
    plt.legend(loc='lower right')
    ax2.plot(nn1, rn1, 'k', label='SOR')
    ax2.plot(nn2, rn2, 'g', label='CG')
    ax2.plot(nn3, rn3, 'b', label='PCG(SSOR)')
    ax2.plot(nn4, rn4, 'r', label='MG')
    ax2.plot(nn5, rn5, 'm', label='PCG(MG)')
    ax2.set_ylabel('Average number of iterations')
    ax2.set_xlim([nn1[0], nn1[-1]])
    ax2.set_xticks(n1)
    ax2.set_xticklabels(n1)
    ax2.set_xscale('log')
    ax2.set_xlim([n1[0], n1[-1]])
    ax2.set_xticks(n1)
    ax2.set_xticklabels(n1)
    ax2.set_xlabel('number of steps')
    ax2.set_title('Average number of iterations before convergence')
    plt.legend(loc='upper left')
    
    plt.tight_layout()
    plt.savefig('result.png')
    plt.savefig('../result.eps', format='eps')
    plt.close()

    f = open('../tables.tex', 'w')
    f.write("\\begin{table}[h!]") 
    f.write("\\begin{tabular}{l | c | c | c | c | r }\n")
    f.write("\\hline\n")
    f.write("Grid size/Time,s & SOR & CG & PCG(SSOR) & PCG(MG) & MG \\\\ \n")
    f.write("\\hline\n")
    for i in range(len(nn1)):
        f.write("%d & %.2lg & %.2lg & %.2lg & %.2lg & %.2lg \\\\ \n" % (n1[i],r1[i],r2[i],r3[i],r5[i],r4[i]))
    f.write("\\hline\n")
    f.write("\\end{tabular}\n")
    f.write("\\caption{Time per one iteration for different methods for 2D heat equation}")
    f.write("\\label{t1}\n");
    f.write("\\end{table}")
    
    f.write("\\begin{table}[h!]") 
    f.write("\\begin{tabular}{l | c | c | c | c | r }\n")
    f.write("\\hline\n")
    f.write("Grid size/Iteration & SOR & CG & PCG(SSOR) & PCG(MG) & MG \\\\ \n")
    f.write("\\hline\n")
    for i in range(len(nn1)):
        f.write("%d & %.2lg & %.2lg & %.2lg & %.2lg & %.2lg \\\\ \n" % (n1[i],rn1[i],rn2[i],rn3[i],rn5[i],rn4[i]))
    f.write("\\hline\n")
    f.write("\\end{tabular}\n")
    f.write("\\caption{Average number of iterations for convergence for 2D heat equation}")
    f.write("\\label{t2}\n");
    f.write("\\end{table}")
    f.close()

if __name__ == "__main__":
    main()
