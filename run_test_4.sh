nmin=8
nmax=63
step=2
n=$nmin

rm "sor.dat" >& /dev/null
rm "cg.dat" >& /dev/null
rm "pcg_ssor.dat" >& /dev/null
rm "pcg_mg.dat" >& /dev/null
rm "mg.dat" >& /dev/null
rm "sorn.dat" >& /dev/null
rm "cgn.dat" >& /dev/null
rm "pcg_ssorn.dat" >& /dev/null
rm "pcg_mgn.dat" >& /dev/null
rm "mgn.dat" >& /dev/null

while [ $n -le $nmax ]
do
    n=$[$n*2]
    size=$[$n+1]
    echo $n $size
    ./solver -nx $size -ny $size -nmax 10 -msolver sor -w 1.26
    ./solver -nx $size -ny $size -nmax 10 -msolver cg
    ./solver -nx $size -ny $size -nmax 10 -msolver pcg 
    ./solver -nx $size -ny $size -nmax 10 -msolver pcg -preconditioner mg
    ./solver -nx $size -ny $size -nmax 10 -msolver mg 
    
    time=`head -1 sor_time.dat | tail -1 `
    echo $n $time >> "sor.dat"
    num=`head -1 sor_num.dat | tail -1 `
    echo $n $num >> "sorn.dat"
    time=`head -1 pcg_ssor_time.dat | tail -1 `
    echo $n $time >> "pcg_ssor.dat"
    num=`head -1 pcg_ssor_num.dat | tail -1 `
    echo $n $num >> "pcg_ssorn.dat"
    time=`head -1 pcg_mg_time.dat | tail -1 `
    echo $n $time >> "pcg_mg.dat"
    num=`head -1 pcg_mg_num.dat | tail -1 `
    echo $n $num >> "pcg_mgn.dat"
    time=`head -1 cg_time.dat | tail -1 `
    echo $n $time >> "cg.dat"
    num=`head -1 cg_num.dat | tail -1 `
    echo $n $num >> "cgn.dat"
    time=`head -1 mg_time.dat | tail -1 `
    echo $n $time >> "mg.dat"
    num=`head -1 mg_num.dat | tail -1 `
    echo $n $num >> "mgn.dat"
done

