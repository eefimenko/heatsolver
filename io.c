#include "header.h"

// Read in parameters from file
void parse_parameters(Parameters *parameters)
{
    char line[256], *s;
    FILE *fp = fopen("parameters", "r");
    // Read string one-by-one and parse
    while((s = fgets(line, sizeof(line), fp)) != NULL){
	if(line[0] == '#') continue; // skip comments
	if(line[0] == '\n') continue; // skip newline
	s = strtok(line, "=");
	if(s == NULL) continue;
        // Number of iterations
	else if(strcmp(s, "nmax") == 0){
	    parameters->num = atoi(strtok(NULL, "=\n"));
	}
        // Step between figure output
	else if(strcmp(s, "save") == 0){
	    parameters->save = atoi(strtok(NULL, "=\n"));
	}
	// Step between profile output
	else if(strcmp(s, "prof") == 0){
	    parameters->prof = atoi(strtok(NULL, "=\n"));
	}
        // Termination criterion: max iterations
	else if(strcmp(s, "maxiteration") == 0){
	    parameters->maxiteration = atoi(strtok(NULL, "=\n"));
	}
	// Number of steps in x dimension
	else if(strcmp(s, "nx") == 0){
	    parameters->nx = atoi(strtok(NULL, "=\n"));
	}
	// Number of steps in y dimension
	else if(strcmp(s, "ny") == 0){
	    parameters->ny = atoi(strtok(NULL, "=\n"));
	}
	// Number of steps in z dimension
	else if(strcmp(s, "nz") == 0){
	    parameters->nz = atoi(strtok(NULL, "=\n"));
	}
        // SOR parameter
	else if(strcmp(s, "w") == 0){
	    parameters->w = atof(strtok(NULL, "=\n"));
	}
	// Size of computational box in x dimension
	else if(strcmp(s, "lx") == 0){
	    parameters->lx = atof(strtok(NULL, "=\n"));
	}
	// Size of computational box in y dimension
	else if(strcmp(s, "ly") == 0){
	    parameters->ly = atof(strtok(NULL, "=\n"));
	}
	// Size of computational box in z dimension
	else if(strcmp(s, "lz") == 0){
	    parameters->lz = atof(strtok(NULL, "=\n"));
	}
	// Velocity in x dimension
	else if(strcmp(s, "vx") == 0){
	    parameters->vx = atof(strtok(NULL, "=\n"));
	}
	// Velocity in y dimension
	else if(strcmp(s, "vy") == 0){
	    parameters->vy = atof(strtok(NULL, "=\n"));
	}
	// Velocity in z dimension
	else if(strcmp(s, "vz") == 0){
	    parameters->vz = atof(strtok(NULL, "=\n"));
	}
        // Temperature at plane x = xmin
	else if(strcmp(s, "txmin") == 0){
	    parameters->txmin = atof(strtok(NULL, "=\n"));
	}
	// Temperature at plane x = xmax
	else if(strcmp(s, "txmax") == 0){
	    parameters->txmax = atof(strtok(NULL, "=\n"));
	}
	// Temperature at plane y = ymin
	else if(strcmp(s, "tymin") == 0){
	    parameters->tymin = atof(strtok(NULL, "=\n"));
	}
	// Temperature at plane y = ymax
	else if(strcmp(s, "tymax") == 0){
	    parameters->tymax = atof(strtok(NULL, "=\n"));
	}
        // Temperature at plane z = zmin
	else if(strcmp(s, "tzmin") == 0){
	    parameters->tzmin = atof(strtok(NULL, "=\n"));
	}
	// Temperature at plane z = zmax
	else if(strcmp(s, "tzmax") == 0){
	    parameters->tzmax = atof(strtok(NULL, "=\n"));
	}
        // Center of initial distribution in x dimension
	else if(strcmp(s, "x0") == 0){
	    parameters->x0 = atof(strtok(NULL, "=\n"));
	}
	// Center of initial distribution in y dimension
	else if(strcmp(s, "y0") == 0){
	    parameters->y0 = atof(strtok(NULL, "=\n"));
	}
	// Center of initial distribution in z dimension
	else if(strcmp(s, "z0") == 0){
	    parameters->z0 = atof(strtok(NULL, "=\n"));
	}
        // Width of initial distribution in x dimension
	else if(strcmp(s, "a") == 0){
	    parameters->a = atof(strtok(NULL, "=\n"));
	}
	// Width of initial distribution in y dimension
	else if(strcmp(s, "b") == 0){
	    parameters->b = atof(strtok(NULL, "=\n"));
	}
	// Width of initial distribution in z dimension
	else if(strcmp(s, "c") == 0){
	    parameters->c = atof(strtok(NULL, "=\n"));
	}
        // Center of source distribution in x dimension
	else if(strcmp(s, "sx0") == 0){
	    parameters->sx0 = atof(strtok(NULL, "=\n"));
	}
	// Center of source distribution in y dimension
	else if(strcmp(s, "sy0") == 0){
	    parameters->sy0 = atof(strtok(NULL, "=\n"));
	}
	// Center of source distribution in z dimension
	else if(strcmp(s, "sz0") == 0){
	    parameters->sz0 = atof(strtok(NULL, "=\n"));
	}
        // Width of source distribution in x dimension
	else if(strcmp(s, "sa") == 0){
	    parameters->sa = atof(strtok(NULL, "=\n"));
	}
	// Width of source distribution in y dimension
	else if(strcmp(s, "sb") == 0){
	    parameters->sb = atof(strtok(NULL, "=\n"));
	}
	// Width of source distribution in z dimension
	else if(strcmp(s, "sc") == 0){
	    parameters->sc = atof(strtok(NULL, "=\n"));
	}
        // Width of source amplitude
	else if(strcmp(s, "samp") == 0){
	    parameters->samp = atof(strtok(NULL, "=\n"));
	}
        // Background temperature
	else if(strcmp(s, "t0") == 0){
	    parameters->t0 = atof(strtok(NULL, "=\n"));
	}
	// Temperature difference in maximum of initial distribution
	else if(strcmp(s, "t1") == 0){
	    parameters->t1 = atof(strtok(NULL, "=\n"));
	}
        // amplitude of the random component(noise)
	else if(strcmp(s, "tr") == 0){
	    parameters->tr = atof(strtok(NULL, "=\n"));
	}
	// Time step
	else if(strcmp(s, "dt") == 0){
	    parameters->dt = atof(strtok(NULL, "=\n"));
	}
	// Diffusivity constant
	else if(strcmp(s, "d") == 0){
	    parameters->d = atof(strtok(NULL, "=\n"));
	}
	// Termination criterion
	else if(strcmp(s, "eps") == 0){
	    parameters->eps = atof(strtok(NULL, "=\n"));
	}
	// Boundary conditions
	else if(strcmp(s, "bc") == 0){
	    s = strtok(NULL, "=\n");
	    if(strcasecmp(s, "constant") == 0)
	    {
		parameters->bc = DIRICHLET;
		parameters->bcx = DIRICHLET;
		parameters->bcy = DIRICHLET;
		parameters->bcz = DIRICHLET;
	    }
	    else if(strcasecmp(s, "periodic") == 0)
	    {
		parameters->bc = PERIODIC;
		parameters->bcx = PERIODIC;
		parameters->bcy = PERIODIC;
		parameters->bcz = PERIODIC;
	    }
	    else
	    {
		print_error("Invalid boundary condition");
	    }
	}
	else if(strcmp(s, "solver") == 0){
	    s = strtok(NULL, "=\n");
	    if(strcasecmp(s, "FTCS") == 0)
	    {
		parameters->solver = FTCS;
	    }
	    else if(strcasecmp(s, "Crank-Nicholson") == 0)
	    {
		parameters->solver = CRNI;
	    }
	    else if(strcasecmp(s, "ADI") == 0)
	    {
		parameters->solver = ADI;
	    }
	    else
	    {
		print_error("Invalid solver");
	    }
	}
	else if(strcmp(s, "msolver") == 0){
	    s = strtok(NULL, "=\n");
	    if(strcasecmp(s, "sor") == 0)
	    {
		parameters->msolver = SOR;
	    }
	    else if(strcasecmp(s, "rref") == 0)
	    {
		parameters->msolver = RREF;
	    }
	    else if(strcasecmp(s, "cg") == 0)
	    {
		parameters->msolver = CG;
	    }
	    else if(strcasecmp(s, "pcg") == 0)
	    {
		parameters->msolver = PCG;
	    }
	    else if(strcasecmp(s, "mg") == 0)
	    {
		parameters->msolver = MG;
	    }
	    else
	    {
		print_error("Invalid matrix solver");
	    }
	}
	else if(strcmp(s, "mgcycle") == 0){
	    s = strtok(NULL, "=\n");
	    if(strcasecmp(s, "fmg") == 0)
	    {
		parameters->mgcycle = FMG;
	    }
	    else if(strcasecmp(s, "vsweep") == 0)
	    {
		parameters->mgcycle = VSWEEP;
	    }
	    else
	    {
		print_error("Invalid mg cycle for MultiGrid solver");
	    }
	}
	else if(strcmp(s, "equation") == 0){
	    s = strtok(NULL, "=\n");
	    if(strcasecmp(s, "diffusion") == 0)
	    {
		parameters->equation = DIFFUSION;
	    }
	    else if(strcasecmp(s, "advection") == 0)
	    {
		parameters->equation = ADVECTION;
	    }
	    else
	    {
		print_error("Invalid equation");
	    }
	}
	else if(strcmp(s, "preconditioner") == 0){
	    s = strtok(NULL, "=\n");
	    if(strcasecmp(s, "jacobi") == 0)
	    {
		parameters->preconditioner = JACOBI_PRE;
	    }
	    else if(strcasecmp(s, "ssor") == 0)
	    {
		parameters->preconditioner = SSOR_PRE;
	    }
	    else if(strcasecmp(s, "mg") == 0)
	    {
		parameters->preconditioner = MG_PRE;
	    }
	    else
	    {
		print_error("Invalid precoditioner");
	    }
	}
	// Unknown config file option
	else print_error("Unknown option in config file.");
    }

    fclose(fp);

    return;
}

void read_command_line(int argc, char *argv[], Parameters *parameters)
{
    int i;
    char *arg;

    // Collect raw input
    for(i=1; i<argc; i++){
	arg = argv[i];
	if(strcmp(arg, "-help") == 0){
	    print_help();
	    exit(0);
	}
        // Number of steps in x,y,z dimensions
	else if(strcmp(arg, "-n") == 0){
	    if(++i < argc)
	    {
		int num = atoi(argv[i]);
		parameters->nx = num;
		parameters->ny = num;
		parameters->nz = num;
	    }
	    else print_error("Error reading command line input '-n'");
	}
        // termination criterion: maxiterations
	else if(strcmp(arg, "-maxiteration") == 0){
	    if(++i < argc) parameters->maxiteration = atoi(argv[i]);
	    else print_error("Error reading command line input '-maxiteration'");
	}
        // Number of steps in x dimension
	else if(strcmp(arg, "-nx") == 0){
	    if(++i < argc) parameters->nx = atoi(argv[i]);
	    else print_error("Error reading command line input '-nx'");
	}
        // Number of steps in y dimension
	else if(strcmp(arg, "-ny") == 0){
	    if(++i < argc) parameters->ny = atoi(argv[i]);
	    else print_error("Error reading command line input '-ny'");
	}
        // Number of steps in z dimension
	else if(strcmp(arg, "-nz") == 0){
	    if(++i < argc) parameters->nz = atoi(argv[i]);
	    else print_error("Error reading command line input '-nz'");
	}
        // Number of iterations
	else if(strcmp(arg, "-nmax") == 0){
	    if(++i < argc) parameters->num = atoi(argv[i]);
	    else print_error("Error reading command line input '-nmax'");
	}
        // Step between figure output
	else if(strcmp(arg, "-save") == 0){
	    if(++i < argc) parameters->save = atoi(argv[i]);
	    else print_error("Error reading command line input '-save'");
	}
        // Step between profile output
	else if(strcmp(arg, "-prof") == 0){
	    if(++i < argc) parameters->prof = atoi(argv[i]);
	    else print_error("Error reading command line input '-prof'");
	}
        //  Size of computational box in x,y,z dimensions
	else if(strcmp(arg, "-l") == 0){
	    if(++i < argc)
	    {
		double length = atof(argv[i]);
		parameters->lx = length;
		parameters->ly = length;
		parameters->lz = length;
	    }
	    else print_error("Error reading command line input '-lx'");
	}
        //  SOR parameter
	else if(strcmp(arg, "-w") == 0){
	    if(++i < argc) parameters->w = atof(argv[i]);
	    else print_error("Error reading command line input '-w'");
	}
        //  Size of computational box in x dimension
	else if(strcmp(arg, "-lx") == 0){
	    if(++i < argc) parameters->lx = atof(argv[i]);
	    else print_error("Error reading command line input '-lx'");
	}
        //  Size of computational box in y dimension
	else if(strcmp(arg, "-ly") == 0){
	    if(++i < argc) parameters->ly = atof(argv[i]);
	    else print_error("Error reading command line input '-ly'");
	}
        //  Size of computational box in z dimension
	else if(strcmp(arg, "-lz") == 0){
	    if(++i < argc) parameters->lz = atof(argv[i]);
	    else print_error("Error reading command line input '-lz'");
	}
	//  Velocity in x dimension
	else if(strcmp(arg, "-vx") == 0){
	    if(++i < argc) parameters->vx = atof(argv[i]);
	    else print_error("Error reading command line input '-vx'");
	}
        //  Velocity in y dimension
	else if(strcmp(arg, "-vy") == 0){
	    if(++i < argc) parameters->vy = atof(argv[i]);
	    else print_error("Error reading command line input '-vy'");
	}
        //  Velocity in z dimension
	else if(strcmp(arg, "-vz") == 0){
	    if(++i < argc) parameters->vz = atof(argv[i]);
	    else print_error("Error reading command line input '-vz'");
	}
	//  Temperature at plane x = xmin
	else if(strcmp(arg, "-txmin") == 0){
	    if(++i < argc) parameters->txmin = atof(argv[i]);
	    else print_error("Error reading command line input '-txmin'");
	}
        //  Temperature at plane x = xmax
	else if(strcmp(arg, "-txmax") == 0){
	    if(++i < argc) parameters->txmax = atof(argv[i]);
	    else print_error("Error reading command line input '-txmax'");
	}
        //  Temperature at plane y = ymin
	else if(strcmp(arg, "-tymin") == 0){
	    if(++i < argc) parameters->tymin = atof(argv[i]);
	    else print_error("Error reading command line input '-tymin'");
	}
        //  Temperature at plane y = ymax
	else if(strcmp(arg, "-tymax") == 0){
	    if(++i < argc) parameters->tymax = atof(argv[i]);
	    else print_error("Error reading command line input '-tymax'");
	}
        //  Temperature at plane z = zmin
	else if(strcmp(arg, "-tzmin") == 0){
	    if(++i < argc) parameters->tzmin = atof(argv[i]);
	    else print_error("Error reading command line input '-tzmin'");
	}
        //  Temperature at plane z = zmax
	else if(strcmp(arg, "-tzmax") == 0){
	    if(++i < argc) parameters->tzmax = atof(argv[i]);
	    else print_error("Error reading command line input '-tzmax'");
	}
        //  Center of initial distribution in x dimension
	else if(strcmp(arg, "-x0") == 0){
	    if(++i < argc) parameters->x0 = atof(argv[i]);
	    else print_error("Error reading command line input '-x0'");
	}
        //  Center of initial distribution in y dimension
	else if(strcmp(arg, "-y0") == 0){
	    if(++i < argc) parameters->y0 = atof(argv[i]);
	    else print_error("Error reading command line input '-y0'");
	}
        //  Center of initial distribution in z dimension
	else if(strcmp(arg, "-z0") == 0){
	    if(++i < argc) parameters->z0 = atof(argv[i]);
	    else print_error("Error reading command line input '-z0'");
	}
        //  Width of initial distribution in x dimension
	else if(strcmp(arg, "-a") == 0){
	    if(++i < argc) parameters->a = atof(argv[i]);
	    else print_error("Error reading command line input '-a'");
	}
        //  Width of initial distribution in y dimension
	else if(strcmp(arg, "-b") == 0){
	    if(++i < argc) parameters->b = atof(argv[i]);
	    else print_error("Error reading command line input '-b'");
	}
        //  Width of initial distribution in z dimension
	else if(strcmp(arg, "-c") == 0){
	    if(++i < argc) parameters->c = atof(argv[i]);
	    else print_error("Error reading command line input '-c'");
	}
        //  Center of source distribution in x dimension
	else if(strcmp(arg, "-sx0") == 0){
	    if(++i < argc) parameters->sx0 = atof(argv[i]);
	    else print_error("Error reading command line input '-sx0'");
	}
        //  Center of source distribution in y dimension
	else if(strcmp(arg, "-sy0") == 0){
	    if(++i < argc) parameters->sy0 = atof(argv[i]);
	    else print_error("Error reading command line input '-sy0'");
	}
        //  Center of source distribution in z dimension
	else if(strcmp(arg, "-sz0") == 0){
	    if(++i < argc) parameters->sz0 = atof(argv[i]);
	    else print_error("Error reading command line input '-sz0'");
	}
        //  Width of source distribution in x dimension
	else if(strcmp(arg, "-sa") == 0){
	    if(++i < argc) parameters->sa = atof(argv[i]);
	    else print_error("Error reading command line input '-sa'");
	}
        //  Width of source distribution in y dimension
	else if(strcmp(arg, "-sb") == 0){
	    if(++i < argc) parameters->sb = atof(argv[i]);
	    else print_error("Error reading command line input '-sb'");
	}
        //  Width of source distribution in z dimension
	else if(strcmp(arg, "-sc") == 0){
	    if(++i < argc) parameters->sc = atof(argv[i]);
	    else print_error("Error reading command line input '-sc'");
	}
        //  Width of source distribution in z dimension
	else if(strcmp(arg, "-samp") == 0){
	    if(++i < argc) parameters->samp = atof(argv[i]);
	    else print_error("Error reading command line input '-samp'");
	}
	//  Background temperature
	else if(strcmp(arg, "-t0") == 0){
	    if(++i < argc) parameters->t0 = atof(argv[i]);
	    else print_error("Error reading command line input '-t0'");
	}
        //  Temperature difference in maximum of initial distribution
	else if(strcmp(arg, "-t1") == 0){
	    if(++i < argc) parameters->t1 = atof(argv[i]);
	    else print_error("Error reading command line input '-t1'");
	}
	//   amplitude of the random component(noise)
	else if(strcmp(arg, "-tr") == 0){
	    if(++i < argc) parameters->tr = atof(argv[i]);
	    else print_error("Error reading command line input '-tr'");
	}
	//  Time step
	else if(strcmp(arg, "-dt") == 0){
	    if(++i < argc) parameters->dt = atof(argv[i]);
	    else print_error("Error reading command line input '-dt'");
	}
        //  Diffusivity constant
	else if(strcmp(arg, "-d") == 0){
	    if(++i < argc) parameters->d = atof(argv[i]);
	    else print_error("Error reading command line input '-d'");
	}
        //  Termination criterion
	else if(strcmp(arg, "-eps") == 0){
	    if(++i < argc) parameters->eps = atof(argv[i]);
	    else print_error("Error reading command line input '-eps'");
	}
	// Boundary conditions (-bc)
	else if(strcmp(arg, "-bc") == 0){
	    if(++i < argc){
		if(strcasecmp(argv[i], "constant") == 0)
		{
		    parameters->bc = DIRICHLET;
		    parameters->bcx = DIRICHLET;
		    parameters->bcy = DIRICHLET;
		    parameters->bcz = DIRICHLET;
		}
		else if(strcasecmp(argv[i], "periodic") == 0)
		{
		    parameters->bc = PERIODIC;
		    parameters->bcx = PERIODIC;
		    parameters->bcy = PERIODIC;
		    parameters->bcz = PERIODIC;
		}
		else
		    print_error("Invalid boundary condition");
	    }
	    else print_error("Error reading command line input '-bc'");
	}
        // Solver (-solver)
	else if(strcmp(arg, "-solver") == 0){
	    if(++i < argc){
		if(strcasecmp(argv[i], "FTCS") == 0)
		    parameters->solver = FTCS;
		else if(strcasecmp(argv[i], "Crank-Nicholson") == 0)
		    parameters->solver = CRNI;
		else if(strcasecmp(argv[i], "ADI") == 0)
		    parameters->solver = ADI;
		else
		    print_error("Invalid solver");
	    }
	    else print_error("Error reading command line input '-solver'");
	}// Matrix solver (-msolver)
	else if(strcmp(arg, "-msolver") == 0){
	    if(++i < argc){
		if(strcasecmp(argv[i], "sor") == 0)
		    parameters->msolver = SOR;
		else if(strcasecmp(argv[i], "rref") == 0)
		    parameters->msolver = RREF;
		else if(strcasecmp(argv[i], "cg") == 0)
		    parameters->msolver = CG;
		else if(strcasecmp(argv[i], "pcg") == 0)
		    parameters->msolver = PCG;
		else if(strcasecmp(argv[i], "mg") == 0)
		    parameters->msolver = MG;
		else
		    print_error("Invalid matrix solver");
	    }
	    else print_error("Error reading command line input '-msolver'");
	}
	else if(strcmp(arg, "-mgcycle") == 0){
	    if(++i < argc){
		if(strcasecmp(argv[i], "fmg") == 0)
		    parameters->mgcycle = FMG;
		else if(strcasecmp(argv[i], "vsweep") == 0)
		    parameters->mgcycle = VSWEEP;
		else
		    print_error("Invalid mg cycle for MultiGrid solver");
	    }
	    else print_error("Error reading command line input '-mgcycle'");
	}
	// Equation (-equation)
	else if(strcmp(arg, "-equation") == 0){
	    if(++i < argc){
		if(strcasecmp(argv[i], "diffusion") == 0)
		    parameters->equation = DIFFUSION;
		else if(strcasecmp(argv[i], "advection") == 0)
		    parameters->equation = ADVECTION;
		else if(strcasecmp(argv[i], "test") == 0)
		    parameters->equation = TEST;
		else
		    print_error("Invalid equation");
	    }
	    else print_error("Error reading command line input '-equation'");
	}
	else if(strcmp(arg, "-preconditioner") == 0){
	    if(++i < argc){
		if(strcasecmp(argv[i], "jacobi") == 0)
		    parameters->preconditioner = JACOBI_PRE;
		else if(strcasecmp(argv[i], "ssor") == 0)
		    parameters->preconditioner = SSOR_PRE;
		else if(strcasecmp(argv[i], "mg") == 0)
		    parameters->preconditioner = MG_PRE;
		else
		    print_error("Invalid preconditioner");
	    }
	    else print_error("Error reading command line input '-preconditioner'");
	}
	// Unknown command line option
	else
	{
	    fprintf(stderr, "%s\n", arg);
	    print_error("Error reading command line input");
	}
    }

    if(parameters->lx <= 0 || parameters->ly <= 0 || parameters->lz <= 0)
    {
	print_error("Length of domain must be positive in x, y, and z dimension");
    }
    if(parameters->nx <= 1 || parameters->ny <= 1 || parameters->nz <= 1)
    {
	print_error("Number of points in x, y, and z dimension must be >= 1");
    }
    if(parameters->dt <= 0)
    {
	print_error("Time step must be > 0");
    }

    return;
}

void print_parameters(Parameters *parameters)
{
  char *bc = NULL;
  char *solver = NULL;
  char *equation = NULL;
  char *preconditioner = NULL;
  char* msolver = NULL;
  char* mgcycle = NULL;

  if(parameters->mgcycle == FMG)
  {
      mgcycle = "FMG";
  }
  else if(parameters->mgcycle == VSWEEP)
  {
      mgcycle = "V-sweep";
  }
  else
  {
      print_error("Unknown mg cycle for MultiGrid solver");
  }

  if(parameters->preconditioner == JACOBI_PRE)
  {
      preconditioner = "Jacobi";
  }
  else if(parameters->preconditioner == SSOR_PRE)
  {
      preconditioner = "SSOR";
  }
  else if(parameters->preconditioner == MG_PRE)
  {
      preconditioner = "MultiGrid";
  }
  else
  {
      print_error("Unknown preconditioner");
  }
  
  if(parameters->bc == DIRICHLET)
  {
      bc = "constant";
  }
  else if(parameters->bc == PERIODIC)
  {
      bc = "periodic";
  }
  else
  {
      print_error("Unknown boundary conditions");
  }

  if(parameters->msolver == SOR)
  {
      msolver = "sor";
  }
  else if(parameters->msolver == RREF)
  {
      msolver = "rref";
  }
  else if(parameters->msolver == CG)
  {
      msolver = "cg";
  }
  else if(parameters->msolver == PCG)
  {
      msolver = "pcg";
  }
  else if(parameters->msolver == MG)
  {
      msolver = "mg";
  }
  else
  {
      print_error("Unknown matrix solver");
  }

  if(parameters->equation == DIFFUSION)
  {
      equation = "Diffusion";
  }
  else if(parameters->equation == ADVECTION)
  {
      equation = "Advection";
  }
  else if(parameters->equation == TEST)
  {
      equation = "Matrix solver test";
  }
  else
  {
      print_error("Unknown equation");
  }
  
  if(parameters->solver == FTCS)
  {
      solver = "FTCS";
  }
  else if(parameters->solver == CRNI)
  {
      solver = "Crank-Nicholson";
  }
  else if(parameters->solver == ADI)
  {
      solver = "ADI";
  }
  else
  {
      print_error("Unknown solver");
  }
  border_print();
  center_print("INPUT SUMMARY", 79);
  border_print();
  printf("Equation:                                 %s\n", equation);
  printf("Solver:                                   %s\n", solver);
  printf("Matrix solver:                            %s\n", msolver);
  if (parameters->msolver == MG)
  {
      printf("MG cycle:                                 %s\n", mgcycle);
  }
  if (parameters->msolver == PCG)
  {
      printf("PCG preconditioner:                       %s\n", preconditioner);
  }
  printf("Number of steps in x dimension:           %d\n", parameters->nx); 
  printf("Number of steps in y dimension:           %d\n", parameters->ny);
  printf("Number of steps in z dimension:           %d\n", parameters->nz); 
  printf("Size of computational box in x dimension: %.2lf\n", parameters->lx); 
  printf("Size of computational box in y dimension: %.2lf\n", parameters->ly);
  printf("Size of computational box in z dimension: %.2lf\n", parameters->lz);
  printf("Size of init.distribution in x dimension: %.2lf\n", parameters->a);
  printf("Size of init.distribution in y dimension: %.2lf\n", parameters->b);
  printf("Size of init.distribution in z dimension: %.2lf\n", parameters->c); 
  printf("Center of init.distribution in z dimension: %.2lf\n", parameters->x0);
  printf("Center of init.distribution in x dimension: %.2lf\n", parameters->y0);
  printf("Center of init.distribution in y dimension: %.2lf\n", parameters->z0);
  printf("Background temperature:                   %.2lf\n", parameters->t0);
  printf("Temperature difference in maximum:        %.2lf\n", parameters->t1); 
  printf("Diffusitivity constant:                   %.2le\n", parameters->d);
  printf("Termination criterion: tolerance          %.2le\n", parameters->eps);
  printf("Termination criterion: max iterations     %d\n", parameters->maxiteration);
  printf("SOR parameter                             %.2le\n", parameters->w);
  printf("Time step:                                %.2le\n", parameters->dt);
  printf("Number of iterations:                     %d\n", parameters->num);
  printf("Step between figure output:               %d\n", parameters->save);
  printf("Step between profile output:              %d\n", parameters->prof);
  printf("Boundary conditions:                      %s\n", bc);
  if (parameters->samp == 0)
  {
      printf("Source:                                   off\n");
  }
  else
  {
      printf("Source amplitude:                           %.2lf\n", parameters->samp);
      printf("Size of source distribution in x dimension: %.2lf\n", parameters->sa);
      printf("Size of source distribution in y dimension: %.2lf\n", parameters->sb);
      printf("Size of source distribution in z dimension: %.2lf\n", parameters->sc); 
      printf("Center of source distribution in z dimension: %.2lf\n", parameters->sx0);
      printf("Center of source distribution in x dimension: %.2lf\n", parameters->sy0);
      printf("Center of source distribution in y dimension: %.2lf\n", parameters->sz0);
  }
  if (parameters->bc == DIRICHLET)
  {
      printf("Temperature at plane x = xmin:            %.2lf\n", parameters->txmin);
      printf("Temperature at plane x = xmax:            %.2lf\n", parameters->txmax);
      printf("Temperature at plane y = ymin:            %.2lf\n", parameters->tymin);
      printf("Temperature at plane y = ymax:            %.2lf\n", parameters->tymax);
      printf("Temperature at plane z = zmin:            %.2lf\n", parameters->tzmin);
      printf("Temperature at plane z = zmax:            %.2lf\n", parameters->tzmax);
  }
  if (parameters->equation == ADVECTION)
  {
      printf("Velocity in x dimension:               %.2lf\n", parameters->vx); 
      printf("Velocity in y dimension:               %.2lf\n", parameters->vy);
      printf("Velocity in z dimension:               %.2lf\n", parameters->vz);
  }
  border_print();
}

void print_error(char *message)
{
  printf("ERROR: %s\n", message);
  exit(1);
}

void border_print(void)
{
  printf( "=========================================="
     "======================================\n");
}

// Prints Section titles in center of 80 char terminal
void center_print(const char *s, int width)
{
  int length = strlen(s);
  int i;
  for (i=0; i<=(width-length)/2; i++){
    fputs(" ", stdout);
  }
  fputs(s, stdout);
  fputs("\n", stdout);
}


void print_help()
{
    border_print();
    printf("Program heatsolver\n");
    printf("List of options:\n");
    printf("-nmax - number of iterations\n");
    printf("-dt - timestep\n");
    printf("-d - diffusivity constant\n");
    printf("-eps - termination criterion: tolerance\n");
    printf("-w - SOR parameter\n");
    printf("-maxiteration - termination criterion: max iterations\n");
    printf("-bc - boundary conditions - constant(default), periodic\n");
    printf("-solver - FTCS(default), Crank-Nicholson, ADI(diffusion)\n");
    printf("-equation diffusion(default), advection\n");
    printf("-save - interval between output, 0 - off(default)\n");
    printf("-prof - interval between profile output, 10(default)\n");
    printf("-nx - number of steps in x dimension\n");
    printf("-ny - number of steps in y dimension\n");
    printf("-nz - number of steps in z dimension\n");
    printf("-n - number of steps in x,y,z dimension\n");
    printf("-lx - size of computational box in x dimension\n");
    printf("-ly - size of computational box in y dimension\n");
    printf("-lz - size of computational box in z dimension\n");
    printf("-vx - velocity in x dimension\n");
    printf("-vy - velocity in y dimension\n");
    printf("-vz - velocity in z dimension\n");
    printf("-l - size of computational box in x,y,z dimension\n");
    printf("-txmin - temperature at x min plane\n");
    printf("-txmax - temperature at x max plane\n");
    printf("-tymin - temperature at y min plane\n");
    printf("-tymax - temperature at y max plane\n");
    printf("-tzmin - temperature at z min plane\n");
    printf("-tzmax - temperature at z max plane\n");
    printf("-x0 - center of initial distribution in x dimension\n");
    printf("-y0 - center of initial distribution in y dimension\n");
    printf("-z0 - center of initial distribution in z dimension\n");
    printf("-a - width of initial distribution in x dimension\n");
    printf("-b - width of initial distribution in y dimension\n");
    printf("-c - width of initial distribution in z dimension\n");
    printf("-t0 - background temperature\n");
    printf("-t1 - temperature difference in maximum of initial distribution\n");
    printf("-tr - amplitude of the random component of temperature(noise)\n");
}

