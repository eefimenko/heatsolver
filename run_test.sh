nmin=5
nmax=200
step=10
n=$nmin

cn_min=8
cn_max=12
cn_step=1
cn=$cn_min
rm "ftcs.dat" >& /dev/null
rm "crni.dat" >& /dev/null
rm "adi.dat" >& /dev/null

while [ $n -le $nmax ]
do
    ./heatsolver -n $n -dt 0.01 -nmax 10
    ./heatsolver -n $n -dt 0.01 -nmax 10 -solver ADI 
    
    n=$[$n+$step]
    time=`head -1 ftcs_time.dat | tail -1 `
    echo $n $time >> "ftcs.dat"
    time=`head -1 adi_time.dat | tail -1 `
    echo $n $time >> "adi.dat"
done

while [ $cn -le $cn_max ]
do
    ./heatsolver -n $cn -dt 0.01 -nmax 10 -solver Crank-Nicholson
    cn=$[$cn+$cn_step]
    time=`head -1 crni_time.dat | tail -1 `
    echo $cn $time >> "crni.dat"
done
