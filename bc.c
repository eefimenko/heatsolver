#include "header.h"

void apply_periodic_bc(field* f_p, Geometry* g)
{
    int i,j,k;
    if (g->bcz == PERIODIC)
    {
	for(i = 0; i<f_p->nx; i++)
	{
	    for(j = 0; j<f_p->ny; j++)
	    {
		f_p->a[INDEX_P(f_p,i,j,0)] = f_p->a[INDEX_P(f_p,i,j,f_p->nz-2)];
		f_p->a[INDEX_P(f_p,i,j,f_p->nz-1)] = f_p->a[INDEX_P(f_p,i,j,1)];
	    }	
	}
    }
    if (g->bcy == PERIODIC)
    {
	for(i = 0; i<f_p->nx; i++)
	{
	    for(k = 0; k<f_p->nz; k++)
	    {
		f_p->a[INDEX_P(f_p,i,0,k)] = f_p->a[INDEX_P(f_p,i,f_p->ny-2,k)];
		f_p->a[INDEX_P(f_p,i,f_p->ny-1,k)] = f_p->a[INDEX_P(f_p,i,1,k)];
	    }	
	}
    }
    if (g->bcx == PERIODIC)
    {
	for(j = 0; j<f_p->ny; j++)
	{
	    for(k = 0; k<f_p->nz; k++)
	    {
		f_p->a[INDEX_P(f_p,0,j,k)] = f_p->a[INDEX_P(f_p,f_p->nx-2,j,k)];
		f_p->a[INDEX_P(f_p,f_p->nx-1,j,k)] = f_p->a[INDEX_P(f_p,1,j,k)];
	    }	
	}
    }
}

void apply_constant_bc(field* f_p, Geometry* g, Parameters* p)
{
    if (g->bc == DIRICHLET)
    {
	int i,j,k;
	for(i = 0; i<f_p->nx; i++)
	{
	    for(j = 0; j<f_p->ny; j++)
	    {
		f_p->a[INDEX_P(f_p,i,j,0)] = p->tzmin;
		f_p->a[INDEX_P(f_p,i,j,f_p->nz-1)] = p->tzmax;
	    }	
	}
	for(i = 0; i<f_p->nx; i++)
	{
	    for(k = 0; k<f_p->nz; k++)
	    {
		f_p->a[INDEX_P(f_p,i,0,k)] = p->tymin;
		f_p->a[INDEX_P(f_p,i,f_p->ny-1,k)] = p->tymax;
	    }	
	}
	for(j = 0; j<f_p->ny; j++)
	{
	    for(k = 0; k<f_p->nz; k++)
	    {
		f_p->a[INDEX_P(f_p,0,j,k)] = p->txmin;
		f_p->a[INDEX_P(f_p,f_p->nx-1,j,k)] = p->txmax;
	    }	
	}
    }
}
