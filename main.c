#include "header.h"

int main(int argc, char *argv[])
{
    Parameters *p; // user defined parameters
    Geometry *g;
    int t;
    double t1 = 0.,t2 = 0.;
/*  set parameters to default values */
    p = init_parameters();
    /* parse parameter file */
    parse_parameters(p);
    /* override with any command line inputs */
    read_command_line(argc, argv, p);
    /* print parameters */
    print_parameters(p);
    /* init geometry */
    g = init_geometry(p);
    
    field u1;
    field *u1_p = &u1;
    /* Init field structure, which stores temperature field,
     this simple structure encapsulates sizes and data for
    reliable access */
    init_field(u1_p, g->nx, g->ny, g->nz);
    /* Sets initial distribution to data */
    set_initial_distribution(u1_p, g, p);
    /* Save field at 0-th iteration */
    save_field(u1_p, 0, g);
   
    // Main loop
    if (p->equation == DIFFUSION)
    {
	if (p->solver == FTCS)
	{
	    field u2;
	    field *u2_p=&u2;
	    field* tmp_p;
	    init_field(u2_p, g->nx, g->ny, g->nz);
	    set_initial_distribution(u2_p, g, p);
	    t1 = timer();
	    for (t=0; t<p->num+1; t++)
	    {
		update_diffusion_ftcs(u1_p,u2_p,g, p);
		tmp_p = u1_p;
		u1_p = u2_p;
		u2_p = tmp_p;
		apply_periodic_bc(u1_p,g);
		save_data(u1_p, t, p, g, t1);
	    }
	    t2 = timer();
	    print_value("ftcs_time.dat", (t2-t1)/p->num);
	    destroy_field(u2_p);
	}
	else if (p->solver == CRNI)
	{
	    if (p->msolver == RREF)
	    {
		matrix a;
		int nx = g->nx-2;
		int ny = g->ny-2;
		int nz = g->nz-2;
		int num = nx * ny * nz;

		init_matrix(&a, num, num + 1);
		initialize_diffusion_crni_scheme(&a, g);
		    
		t1 = timer();
		for (t=0; t<p->num+1; t++)
		{
		    update_diffusion_matrix_with_field(&a, u1_p, g, p);
		    initialize_diffusion_crni_scheme(&a, g);
		    rref(&a);
		    extract_field_from_matrix(&a, u1_p, g);
		    apply_periodic_bc(u1_p,g);
		    save_data(u1_p, t, p, g, t1);
		}
		t2 = timer();
		print_value("rref_time.dat", (t2-t1)/p->num);
		destroy_matrix(&a);
	    }
	    else if (p->msolver == SOR)
	    {
		matrix a;
		int nx = g->nx-2;
		int ny = g->ny-2;
		int nz = g->nz-2;
		int num = nx * ny * nz;
		
		init_matrix(&a, num, num + 1);
		initialize_diffusion_crni_scheme(&a, g);
	
		double *x = init_vector(num);
		double av_num = 0;

		t1 = timer();
		for (t=0; t<p->num+1; t++)
		{
		    update_diffusion_matrix_with_field(&a, u1_p, g, p);
		    memset(x,0,num*sizeof(double));
		    int n_ = sor(&a, x, p->eps, p->maxiteration, p->w);
		    av_num += n_;
		    read_field_from_vector(u1_p,x,g);
		    apply_periodic_bc(u1_p,g);
		    save_data(u1_p, t, p, g, t1);
		}
		t2 = timer();
		printf("Avegare number of iterations: %.0lf\n", av_num/(p->num+1));
	    
		print_value("sor_time.dat", (t2-t1)/p->num);
		print_value("sor_num.dat", av_num/(p->num+1));
	    
		destroy_matrix(&a);
		free(x);
	    }
	    else if (p->msolver == CG)
	    {
		matrix a;
		int nx = g->nx-2;
		int ny = g->ny-2;
		int nz = g->nz-2;
		int num = nx * ny * nz;

		init_matrix(&a, num, num + 1);
		initialize_diffusion_crni_scheme(&a, g);
			
		double* b = init_vector(num);
		double* s = init_vector(num);
		double* r = init_vector(num);
		double* ap = init_vector(num);

		double *x = init_vector(num);
		double av_num = 0;

		t1 = timer();
		for (t=0; t<p->num+1; t++)
		{
		    update_diffusion_matrix_with_field(&a, u1_p, g, p);
		    int n_ = cg(&a, x, b, s, r, ap, p->eps, p->maxiteration);
		    av_num += n_;
		    read_field_from_vector(u1_p,x,g);
		    apply_periodic_bc(u1_p,g);
		    save_data(u1_p, t, p, g, t1);
		}
		t2 = timer();
		printf("Avegare number of iterations: %.0lf\n", av_num/(p->num+1));
	    
		print_value("cg_time.dat", (t2-t1)/p->num);
		print_value("cg_num.dat", av_num/(p->num+1));
	    
		destroy_matrix(&a);
	    
		free(x);
		free(ap);
		free(b);
		free(r);
		free(s);
	    }
	    else if (p->msolver == PCG)
	    {
		matrix a;
		int nx = g->nx-2;
		int ny = g->ny-2;
		int nz = g->nz-2;
		int num = nx * ny * nz;

		init_matrix(&a, num, num + 1);
		initialize_diffusion_crni_scheme(&a, g);
			
		double* b = init_vector(num);
		double* s = init_vector(num);
		double* r = init_vector(num);
		double* z = init_vector(num);
		double* ap = init_vector(num);
		
		double *x = init_vector(num);
		Preconditioner pre;
		init_preconditioner(&pre, u1_p, &a, p->w, g, p);
		double av_num = 0;
		
		t1 = timer();
		for (t=0; t<p->num+1; t++)
		{
		    update_diffusion_matrix_with_field(&a, u1_p, g, p);
		    int n_ = pcg(&a, x, b, r, z, s, ap, p->eps, p->maxiteration, p->w, &pre, g, p);
		    av_num += n_;
		    read_field_from_vector(u1_p,x,g);
		    apply_periodic_bc(u1_p,g);
		    save_data(u1_p, t, p, g, t1);
		}
		t2 = timer();
		printf("Avegare number of iterations: %.0lf\n", av_num/(p->num+1));

		char name[128];
		char* pre_ = NULL;
		if (p->preconditioner == SSOR_PRE)
		{
		    pre_ = "ssor";
		}
		else if (p->preconditioner == MG_PRE)
		{
		    pre_ = "mg";
		}
		else
		{
		    pre_ = "jacobi";
		}
		sprintf(name, "pcg_%s_time.dat", pre_);
		print_value(name, (t2-t1)/p->num);
		sprintf(name, "pcg_%s_num.dat", pre_);
		print_value(name, av_num/(p->num+1));
		
		destroy_matrix(&a);
		destroy_preconditioner(&pre, p);
		free(x);
		free(ap);
		free(b);
		free(r);
		free(s);
		free(z);
	    }
	    else if (p->msolver == MG)
	    {
		Multigrid m;
		init_multigrid(&m, u1_p, g, p);
		double av_num = 0;
		t1 = timer();
		for (t=0; t<p->num+1; t++)
		{
		    /* Full Multi Grid method is implemented here */
		    if (p->mgcycle == FMG)
		    {
			int n_ = fmg(&m, g, p, p->eps, p->maxiteration);
			av_num += n_;	
		    }
		    else if (p->mgcycle == VSWEEP)
		    {
			int n_ = vsweep(&m, g, p, p->eps, p->maxiteration);
			av_num += n_;
		    }
		    else
		    {
			printf("Unknown cycle for MultiGrid Solver\n");
			exit(-1);
		    }
		    read_field_from_vector(&m.u1[m.ng-1],m.x[m.ng-1],g);
		    apply_periodic_bc(&m.u1[m.ng-1],g);
		    save_data(&m.u1[m.ng-1], t, p, g, t1);
		}
		printf("Avegare number of iterations: %.0lf\n", av_num/(p->num+1));
		t2 = timer();
		print_value("mg_time.dat", (t2-t1)/p->num);
		print_value("mg_num.dat", av_num/(p->num+1));
		destroy_multigrid(&m);
	    }
	    else if (p->msolver == MG_OLD)
	    {
		int nn,ng=0;
		
		if (g->nx != g->ny)
		{
		    printf("Error: grid must be square\n");
		    exit(-1);
		}
		int n = g->nx; // size of the grid
		    
		/*** use bitshift to find the number of grid levels, stored in ng ***/
		nn=n;                   
		while (nn >>= 1) ng++;     
			
		/*** some simple input checks ***/
		if (n != 1+(1L << ng))
		{
		    printf("n-1 must be a power of 2\n");
		    exit(-1);
		}
	
		printf("Number of grids: %d\n", ng);
		int i;	
		matrix a[ng];
		field u1[ng];
		field u10[ng];
		field ures[ng];
		int mult[ng];
		int num[ng];
		int gridsize[ng];
		double* x[ng];
		double* res[ng];
		
		/* First perform initialisation of all variables for all grids*/
		for (i=0; i<ng; i++)
		{
		    int d = (ng-1)-i; 
		    mult[i] = pow(2,d); // this number shows how many times step on i-th grid greater than fine grid step
		    int n1 = (n-1)/pow(2,d)-1;
		    num[i] = n1*n1;
		    gridsize[i] = n1+2; // number of step in i-th grid
		    init_matrix(&a[i],num[i],num[i]+1); //initialise matrix for i-th grid
		    initialize_diffusion_mg_scheme(&a[i], g, mult[i]); // fill in Crank-Nicholson coefficients
		    x[i] = init_vector(num[i]); // init vector for solution on i-th grid
		    res[i] = init_vector(num[i]); //init vector for residual on i-th grid 
		    init_field(&u1[i], n1+2, n1+2, 3); // init field on i-th grid
		    init_field(&u10[i], n1+2, n1+2, 3); // init auxilary field on i-th grid, that stores initial field value
		    init_field(&ures[i], n1+2, n1+2, 3); // init auxilary field on i-th grid which stores residual
//		    set_initial_distribution_mg(&u1[i], g, p, mult[i]); // set initial distribution
		}
		copy_field(&u1[ng-1], u1_p);
		int k = 0, vcycle;
			
		t1 = timer();
		for (t=0; t<p->num+1; t++)
		{
		    /* Full Multi Grid method is implemented here */
		    if (p->mgcycle == FMG)
		    {
			/* 1. First, interpolate to the coarsest grid */
			copy_field(&u10[ng-1], &u1[ng-1]);
			for (i=ng-1; i>0; i--)
			{
			    rstrct(&u10[i-1], &u10[i], gridsize[i-1]);
			}
			update_diffusion_matrix_with_field_mg(&a[0], &u10[0], g, p, mult[0], 1.);
			/* Solve the equation on the coarsest grid*/
			sor(&a[0], x[0], p->eps, p->maxiteration, 1.);
			read_field_from_vector(&u1[0],x[0],g);

                        /* 2. Perform a number of V-cycles, starting from the coarsest grids */ 
			for (vcycle=1; vcycle<ng; vcycle++)
			{
			    interp(&u1[vcycle],&u1[vcycle-1],gridsize[vcycle]);
			    read_vector_from_field(&u1[vcycle], x[vcycle], g);
			    update_diffusion_matrix_with_field_mg(&a[vcycle], &u10[vcycle], g, p, mult[vcycle],1.);
			    for (i = vcycle; i>k; i--)
			    {
				sor(&a[i], x[i], p->eps, NPRE, 1.); // pre smoothing
				residual(&a[i], x[i], res[i], num[i]); // calculating residual
				read_field_from_vector(&ures[i], res[i], g);
				read_field_from_vector(&u1[i], x[i], g); // saving solution
				rstrct(&u1[i-1], &ures[i], gridsize[i-1]); // restiction to a coarser grid
				update_diffusion_matrix_mg(&a[i-1], &u1[i-1], g, p); // update rhs
			    }
		   
			    sor(&a[k], x[k], p->eps, p->maxiteration, 1.); // solving eqaution on the coarsest grid
		   	
			    for (i = k+1; i < vcycle+1; i++)
			    {
				read_field_from_vector(&u1[i-1],x[i-1],g);
				addint(&u1[i],&u1[i-1],&ures[i],gridsize[i]); //interpolation and adding error to a finer grid
				sor(&a[i], x[i], p->eps, NPOST, 1.); //post smoothing
			    }
			    read_field_from_vector(&u1[vcycle],x[vcycle],g);
			}
			
		    }
		    /* Alternative version based on V-sweep algorithm */
		    else if (p->mgcycle == VSWEEP)
		    {
			//update rhs on the finest grid
			update_diffusion_matrix_with_field_mg(&a[ng-1], &u1[ng-1], g, p, mult[ng-1],1.); 
			for (i = ng-1; i>k; i--)
			{
			    sor(&a[i], x[i], p->eps, NPRE, 1.); // perform pre smoothing, few GS iterations
			    residual(&a[i], x[i], res[i], num[i]); // calculate residual b - Ax
			    read_field_from_vector(&ures[i], res[i], g); // read residual into matrix
			    read_field_from_vector(&u1[i], x[i], g); // save field into matrix
			    rstrct(&u1[i-1], &ures[i], gridsize[i-1]); // restrict difference to coarser grid
			    update_diffusion_matrix_mg(&a[i-1], &u1[i-1], g, p); //update rhs 
			}
			// solve on the coarsest grid, few GS iterations
			sor(&a[k], x[k], p->eps, p->maxiteration, 1.);
		   		   
			for (i = k+1; i < ng; i++)
			{
			    read_field_from_vector(&u1[i-1],x[i-1],g);
			    // interpolate and add sifference to solution on the upper layer
			    addint(&u1[i],&u1[i-1],&ures[i],gridsize[i]);
			    sor(&a[i], x[i], p->eps, NPOST, 1.); //post smoothing
			}
		    }
		    else
		    {
			printf("Unknown cycle for MultiGrid Solver\n");
			exit(-1);
		    }
		    read_field_from_vector(&u1[ng-1],x[ng-1],g);
		    apply_periodic_bc(&u1[ng-1],g);
		    save_data(&u1[ng-1], t, p, g, t1);
		}
		t2 = timer();
		print_value("mg_time.dat", (t2-t1)/p->num);

		for (i=0; i<ng; i++)
		{
		    destroy_matrix(&a[i]);
		    free(x[i]);
		    free(res[i]);
		    destroy_field(&u1[i]);
		    destroy_field(&u10[i]);
		    destroy_field(&ures[i]);
		}
	    }
	    else
	    {
		fprintf(stderr, "Unknown matrix solver\n");
		exit(-1);
	    }
	}
	else if (p->solver == ADI)
	{
	    matrix3 ax,ay,az;
	    
	    int nx = g->nx-2;
	    int ny = g->ny-2;
	    int nz = g->nz-2;
	    
	    int num = nx*ny*nz;
	    
	    init_matrix3(&ax, num, g->ax);
	    init_matrix3(&ay, num, g->ay);
	    init_matrix3(&az, num, g->az);
	    
	    initialize_adi_scheme(&ax, g->ax, g);
	    initialize_adi_scheme(&ay, g->ay, g);
	    initialize_adi_scheme(&az, g->az, g);
	    t1 = timer();
	    for (t=0; t<p->num+1; t++)
	    {
		update_matrix3_with_field(&ax, u1_p, g, DX(u1_p), g->ax, p);
		solve(&ax);
		extract_field_from_matrix3(&ax, u1_p, g);
		update_matrix3_with_field(&ay, u1_p, g, DY(u1_p), g->ay, p);
		solve(&ay);
		extract_field_from_matrix3(&ay, u1_p, g);
		update_matrix3_with_field(&az, u1_p, g, DZ(u1_p), g->az, p);
		solve(&az);
		extract_field_from_matrix3(&az, u1_p, g);
		apply_periodic_bc(u1_p,g);
		save_data(u1_p, t, p, g, t1);
	    }
	    t2 = timer();
	    print_value("adi_time.dat", (t2-t1)/p->num);
	    destroy_matrix3(&ax);
	    destroy_matrix3(&ay);
	    destroy_matrix3(&az);
	}
	else
	{
	    printf("Unknown solver\n");
	}
    }
    else if (p->equation == ADVECTION)
    {
	printf("Advection equation\n");
	if (p->solver == FTCS)
	{
	    field u2;
	    field *u2_p=&u2;
	    field* tmp_p;
	    
	    init_field(u2_p, g->nx, g->ny, g->nz);
	    set_initial_distribution(u2_p, g, p);
	    t1 = timer();
	    for (t=0; t<p->num+1; t++)
	    {
		//print_field(u1_p);
		update_advection_ftcs(u1_p,u2_p,g, p);
		tmp_p = u1_p;
		u1_p = u2_p;
		u2_p = tmp_p;
		apply_periodic_bc(u1_p,g);
		save_data(u1_p, t, p, g, t1);
	    }
	    t2 = timer();
	    print_value("ftcs_time.dat", (t2-t1)/p->num);
	    destroy_field(u2_p);
	}
	else if (p->solver == CRNI)
	{
	    matrix a;

	    int nx = g->nx-2;
	    int ny = g->ny-2;
	    int nz = g->nz-2;

	    int num = nx * ny * nz;

	    init_matrix(&a, num, num + 1);
	   
	    double *x = init_vector(num);
	    initialize_advection_crni_scheme(&a, g);  
	    t1 = timer();
	    
	    for (t=0; t<p->num+1; t++)
	    {
		update_advection_matrix_with_field(&a, u1_p, g, p);

		if (p->msolver == RREF)
		{
		    initialize_advection_crni_scheme(&a, g);
		    rref(&a);
		    extract_field_from_matrix(&a, u1_p, g);
		}
		else if (p->msolver == SOR)
		{
		    memset(x,0,num*sizeof(double));
		    sor(&a, x, p->eps, p->maxiteration, p->w);
		    read_field_from_vector(u1_p,x,g);
		}
		else
		{
		    fprintf(stderr, "Unknown matrix solver\n");
		    exit(-1);
		}
		
		apply_periodic_bc(u1_p,g);
		save_data(u1_p, t, p, g, t1);
	    }
	    t2 = timer();
	    print_value("crni_time.dat", (t2-t1)/p->num);
	    destroy_matrix(&a);
	    free(x);
	}
	else
	{
	    printf("Unknown solver\n");
	}
    }
    else if (p->equation == TEST)
    {
	matrix m,r,p;
	int n = 4;
	int i;
	init_matrix(&m, n, n + 1);
	init_matrix(&r, n, n);
	init_matrix(&p, n, n);

	printf("Test matrix solvers\n");
	m.a[IDX(&m,0,0)] = 1.; m.a[IDX(&m,0,1)] = 0.1; m.a[IDX(&m,0,2)] = 0.5; m.a[IDX(&m,0,3)] = 0.3;m.a[IDX(&m,0,4)] = 3.;
	m.a[IDX(&m,1,0)] = 0.1; m.a[IDX(&m,1,1)] = 1.; m.a[IDX(&m,1,2)] = 0.1; m.a[IDX(&m,1,3)] = 0.7;m.a[IDX(&m,1,4)] = 6.;
	m.a[IDX(&m,2,0)] = 0.5; m.a[IDX(&m,2,1)] = 0.1; m.a[IDX(&m,2,2)] = 1.; m.a[IDX(&m,2,3)] = 0.1;m.a[IDX(&m,2,4)] = 9.;
	m.a[IDX(&m,3,0)] = 0.3; m.a[IDX(&m,3,1)] = 0.2; m.a[IDX(&m,3,2)] = 0.1; m.a[IDX(&m,3,3)] = 1.;m.a[IDX(&m,3,4)] = 12;
	printf("Initial matrix:\n");
	print_matrix(&m);
	rref(&m);
	print_matrix(&m);
	double *x;
	x = (double*)malloc(sizeof(double)*n);
	memset(x,0,sizeof(double)*n);
	m.a[IDX(&m,0,0)] = 1.; m.a[IDX(&m,0,1)] = 0.1; m.a[IDX(&m,0,2)] = 0.5; m.a[IDX(&m,0,3)] = 0.3;m.a[IDX(&m,0,4)] = 3.;
	m.a[IDX(&m,1,0)] = 0.1; m.a[IDX(&m,1,1)] = 1.; m.a[IDX(&m,1,2)] = 0.1; m.a[IDX(&m,1,3)] = 0.7;m.a[IDX(&m,1,4)] = 6.;
	m.a[IDX(&m,2,0)] = 0.5; m.a[IDX(&m,2,1)] = 0.1; m.a[IDX(&m,2,2)] = 1.; m.a[IDX(&m,2,3)] = 0.1;m.a[IDX(&m,2,4)] = 9.;
	m.a[IDX(&m,3,0)] = 0.3; m.a[IDX(&m,3,1)] = 0.2; m.a[IDX(&m,3,2)] = 0.1; m.a[IDX(&m,3,3)] = 1.;m.a[IDX(&m,3,4)] = 12;
	print_matrix(&m);
	sor(&m, x, 1e-9, 1000, 1.);
	for (i = 0; i<n; i++)
	{
	    printf("%lf\n", x[i]);
	}
//	cg(&m, x, 1e-3, 100);
	for (i = 0; i<n; i++)
	{
	    printf("%lf\n", x[i]);
	}
	double* b = (double *) malloc(n*sizeof(double));
	b[0] = 1.; b[1] = 1.; b[2] = -1; b[3] = 3;
	matvec(x, &m, b,n);
	print_v("x", x, n);
	destroy_matrix(&m);
	r.a[IDX(&r,0,0)] = 1.; r.a[IDX(&r,0,1)] = 0.1; r.a[IDX(&r,0,2)] = 0.5; r.a[IDX(&r,0,3)] = 0.3;
	r.a[IDX(&r,1,0)] = 0.1; r.a[IDX(&r,1,1)] = 1.; r.a[IDX(&r,1,2)] = 0.1; r.a[IDX(&r,1,3)] = 0.7;
	r.a[IDX(&r,2,0)] = 0.5; r.a[IDX(&r,2,1)] = 0.1; r.a[IDX(&r,2,2)] = 1.; r.a[IDX(&r,2,3)] = 0.1;
	r.a[IDX(&r,3,0)] = 0.3; r.a[IDX(&r,3,1)] = 0.2; r.a[IDX(&r,3,2)] = 0.1; r.a[IDX(&r,3,3)] = 1.;
	mdot(&r, &r, &p);
	print_matrix(&p);
	inv(&p, &r, n);
	print_matrix(&r);
	destroy_matrix(&p);
	destroy_matrix(&r);
	free(x);
    }
    else
    {
	printf("Unknown equation %d\n", p->equation);
    }
   
    printf("Time %lf s\n", t2-t1);
    save_field(u1_p, p->num, g);
        
    destroy_field(u1_p);
   
    free(p);
    free(g);
    return 0;
}
