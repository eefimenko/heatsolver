#include "header.h"

void init_aug_matrix(matrix* m, matrix* aug, int n)
{
    int i,j;
    for (i = 0; i<n; i++)
    {
	for (j = 0; j<n; j++)
	{
	    aug->a[IDX(aug,i,j)] = m->a[IDX(m,i,j)];
	    if (i == j)
	    {
		aug->a[IDX(aug,i,j+n)] = 1.;
	    }
	}  
    }
}

void inv(matrix* m, matrix* inv, int n)    
{
    
    int i,j,k,temp;
    double temporary,r;
    int nr = n;
    matrix aug;
    printf("Enter inv\n");
    init_matrix(&aug, n, 2*n);
    init_aug_matrix(m, &aug, n);
    
    for(j=0; j<nr; j++)
    {
	temp=j;
	/* finding maximum jth column element in last (dimension-j) rows */
	for(i=j+1; i<nr; i++)
	{
	    if(aug.a[IDX(&aug,i,j)] > aug.a[IDX(&aug,temp,j)])
	    {
		temp=i;
	    }
	}
	/* swapping row which has maximum jth column element */
	if(temp != j)
	{  
            for(k=0; k<2*nr; k++)
            {
		temporary = aug.a[IDX(&aug,j,k)];
		aug.a[IDX(&aug,j,k)] = aug.a[IDX(&aug,temp,k)];
		aug.a[IDX(&aug,temp,k)] = temporary;
            }
	}

/* performing row operations to form required identity matrix out of the input matrix */

	for(i=0; i<nr; i++)
	{
            if(i != j)
            {
		r = aug.a[IDX(&aug,i,j)];
		for(k=0; k<2*nr; k++)
		{
		    aug.a[IDX(&aug,i,k)] -= (aug.a[IDX(&aug,j,k)] / aug.a[IDX(&aug,j,j)])*r;
		}
            }
            else
            {
		r = aug.a[IDX(&aug,i,j)];
		for(k=0; k<2*nr; k++)
		{
		    aug.a[IDX(&aug,i,k)] /= r;
		}
            }
	}

    }
    for (i = 0; i<n; i++)
    {
	for (j = 0; j<n; j++)
	{
	    inv->a[IDX(inv,i,j)] = aug.a[IDX(&aug,i,j+n)];
	}  
    }
    printf("Exit inv\n");
}

void mdot(matrix* A, matrix* B, matrix* C)
{
    int nx1 = A->nx;
    int ny1 = A->ny;
    int nx2 = B->nx;
    int ny2 = B->ny;
    int nx3 = C->nx;
    int ny3 = C->ny;
    int i,j,k;
    zero_matrix(C);
    printf("Enter mdot\n");
    if (ny1 != nx2 || nx1 != nx3 || ny2 != ny3)
    {
	fprintf(stderr, "Wrong matrix sizes: %dx%d %dx%d %dx%d\n", nx1, ny1, nx2, ny2, nx3, ny3);
	exit(-1);
    }
    
    for (i = 0; i < nx3; i++)
    {
	for(j = 0; j < ny3; j++)
	{
	    for (k = 0; k< ny1; k++)
	    {
		C->a[IDX(C,i,j)] += A->a[IDX(A,i,k)] * B->a[IDX(B,k,j)];
	    }
	}
    }
    printf("Exit mdot\n");
}

void sum_vectors(double *res, double m1, double m2, double *x1, double *x2, int n)
{
    int i;
    for (i = 0; i < n; i++)
        res[i] = m1 * x1[i] + m2 * x2[i];
}

void update_vectors(double *res, double m, double *x, int n)
{
    int i;
    for (i = 0; i < n; i++)
        res[i] += m * x[i];
}

/* here we assume that matrix is square */
void matvec(double *res, matrix* A, double* vec, int n)
{
    int i, j;		
    memset(res, 0, sizeof(double)*n);
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
	{
            res[i] += A->a[IDX(A,i,j)]*vec[j];
        }
    }
}

/* Scalar product of two vectors */
double ddot(double *x, double *y, int n)
{
    int i;
    double sum = 0;

    for (i = 0; i < n; i++)
    {
        sum += x[i] * y[i];
    }

    return sum;
}

void print_v(char* name, double* v, int n)
{
    int j;
    double max = 0;
    for (j=0; j< n; j++)
    {
	printf("%s[%d] = %lg\n", name, j, v[j]);
	if (v[j]> max)
	{
	    max = v[j];
	}
    }
    printf("max %s = %lf\n", name, max);
}

int zero_matrix(matrix* m_p)
{
    int bytes = sizeof(double)*m_p->nx*m_p->ny;
    memset(m_p->a, 0, bytes);
    return 0;
}

/*** copy in into out ***/
void copy_matrix(matrix *out, matrix *in)
{
    int bytes_in = sizeof(double)*in->nx*in->ny;
    int bytes_out = sizeof(double)*out->nx*out->ny;
    if (bytes_in != bytes_out)
    {
	printf("Matrices have different size\n");
	exit(-1);
    }
    memcpy(out->a, in->a, bytes_in);
}

int init_matrix(matrix* m_p, int nx, int ny)
{
    unsigned int bytes = sizeof(double)*nx*ny;
    m_p->nx = nx;
    m_p->ny = ny;

    if (0 == bytes)
    {
        m_p->a = NULL;
        return 0;
    }

    m_p->a = (double*)malloc(bytes);

    if (NULL == m_p->a)
    {
        fprintf(stderr, "Not enough memory %d bytes: %d %d\n", bytes, nx, ny);
	fflush(stderr);
        exit(-1);
    }
    memset(m_p->a, 0, bytes);

    return 0;
}

void print_matrix(matrix* m_p)
{
    int i,j;
    for (i=0; i<m_p->nx; i++)
    {
	for (j=0; j<m_p->ny; j++)
	{
	    printf("%lg\t", m_p->a[IDX(m_p,i,j)]);
	}
	printf("\n");
    }
}

void destroy_matrix(matrix* m_p)
{
    if (0 == m_p->nx ||	0 == m_p->ny ||	NULL == m_p->a)
    {
        return;
    }

    free(m_p->a);
    m_p->a = NULL;
}

void rref(matrix* m_p)
{
    int nr = m_p->nx;
    int nc = m_p->ny;
    int i,j,k;
    for(i = 0; i < nr - 1; i++)
    {
        for(j = i+1; j < nr; j++)
        {
            //Finding pivot
            double pivot = m_p->a[IDX(m_p,i,i)];
            int index = i;
	    
            for(k = i+1; k < nr; k++)
            {
                if(abs(pivot) < abs(m_p->a[IDX(m_p,k,i)]))
                {
                    index = k;
                    pivot = m_p->a[IDX(m_p,k,i)];
                }
            }
	    if (pivot == 0)
	    {
		continue;
	    }
            //Row exchange
	    if (i != index)
	    {
		for(k = 0; k < nc; k++)
		{
		    int i1 = IDX(m_p,i,k);
		    int i2 = IDX(m_p,index,k);
		    double tmp = m_p->a[i1];
		    m_p->a[i1] = m_p->a[i2];
		    m_p->a[i2] = tmp;
		}
	    }

            //Elimination
	    double coefficient = -(m_p->a[IDX(m_p,j,i)]/m_p->a[IDX(m_p,i,i)]);
            for(k = i; k < nc; k++)
            {
                m_p->a[IDX(m_p,j,k)] += coefficient*m_p->a[IDX(m_p,i,k)];
            }
        }
    }

    //Back-substitution
    int r = nc;
    int i0 = nc;
    if (nr < nc)
    {
	r = nr;
    }
    for (i = r-1; i>=0; i--)
    {
	if(m_p->a[IDX(m_p,i,i)] != 0)
	{
	    i0 = i;
	    break;
	}
    }

    double coeff = m_p->a[IDX(m_p,i0,i0)];
    for(j = i0; j < nc; j++)
    {
	m_p->a[IDX(m_p,i0,j)] /= coeff;
    }
    
    for(i = i0-1; i>=0; i--)
    {
	double coeff = m_p->a[IDX(m_p,i,i)];
	for(j = i; j < nc; j++)
        {
	    m_p->a[IDX(m_p,i,j)] /= coeff;
	}
	
	for (k = 0; k <= i; k++)
	{
	    for(j = nc-1; j >= i; j--)
	    {
		m_p->a[IDX(m_p,k,j)] -= m_p->a[IDX(m_p,i+1,j)]*m_p->a[IDX(m_p,k,i+1)];
	    }
	}
    }
}

void extract_field_from_matrix(matrix* m_p, field* f_p, Geometry* g)
{
    int i,j,k=0;
    int idx = 0;
    for (k = 1; k<g->nz-1; k++)
    {
	for (j = 1; j<g->ny-1; j++)
	{
	    for (i = 1; i<g->nx-1; i++)
	    {
		f_p->a[INDEX_P(f_p,i,j,k)] = m_p->a[IDX(m_p,idx,m_p->ny-1)];
		idx++;
	    }
	}
    }
}

void write_field_to_vector(field* f_p, double* x, Geometry* g)
{
    int i,j,k;
    int idx = 0;
    for (k = 1; k<g->nz-1; k++)
    {
	for (j = 1; j<g->ny-1; j++)
	{
	    for (i = 1; i<g->nx-1; i++)
	    {
		x[idx] = f_p->a[INDEX_P(f_p,i,j,k)];
		idx++;
	    }
	}
    }
}

double* init_vector(int num)
{
    double *x = (double*)malloc(num*sizeof(double));
    if (NULL == x)
    {
	fprintf(stderr, "Unable to allocate memory\n");
	exit(-1);
    }
    memset(x,0,num*sizeof(double));
    return x;
}

void read_field_from_vector(field* f_p, double* x, Geometry* g)
{
    int i,j,k;
    int idx = 0;
    for (k = 1; k<f_p->nz-1; k++)
    {
	for (j = 1; j<f_p->ny-1; j++)
	{
	    for (i = 1; i<f_p->nx-1; i++)
	    {
		f_p->a[INDEX_P(f_p,i,j,k)] = x[idx];
		idx++;
	    }
	}
    }
}

void read_vector_from_field(field* f_p, double* x, Geometry* g)
{
    int i,j,k;
    int idx = 0;
    for (k = 1; k<f_p->nz-1; k++)
    {
	for (j = 1; j<f_p->ny-1; j++)
	{
	    for (i = 1; i<f_p->nx-1; i++)
	    {
		x[idx] = f_p->a[INDEX_P(f_p,i,j,k)];
		idx++;
	    }
	}
    }
}

void fill_diffusion_matrix(matrix* m_p, field* f_p, Geometry* g, Parameters* p)
{
    int i,j;
    for (j = 1; j<g->ny-1; j++)
    {
	for (i = 1; i<g->nx-1; i++)
	{
	    int index = INDEX_P(f_p,i,j,1);
	    m_p->a[IDX(m_p,i,j)] =
		0.5*g->ax * f_p->a[index - DX(f_p)] +
		0.5*g->ay * f_p->a[index - DY(f_p)] +
		(1. - g->ax - g->ay) * f_p->a[index] +
		0.5*g->ax * f_p->a[index + DX(f_p)] +
		0.5*g->ay * f_p->a[index + DY(f_p)] +
		+ g->b*source(i,j,0,g,p);
	    
	}
    }
}

void update_diffusion_matrix_with_field(matrix* m_p, field* f_p, Geometry* g, Parameters* p)
{
    int i,j,k;
    int idx = 0;
    for (k = 1; k<f_p->nz-1; k++)
    {
	for (j = 1; j<f_p->ny-1; j++)
	{
	    for (i = 1; i<f_p->nx-1; i++)
	    {
		int index = INDEX_P(f_p,i,j,k);
		m_p->a[IDX(m_p,idx,m_p->ny-1)] =
		    0.5*g->ax * f_p->a[index - DX(f_p)] +
		    0.5*g->ay * f_p->a[index - DY(f_p)] +
		    0.5*g->az * f_p->a[index - DZ(f_p)] +
		    (1. - g->ax - g->ay - g->az) * f_p->a[index] +
		    0.5*g->ax * f_p->a[index + DX(f_p)] +
		    0.5*g->ay * f_p->a[index + DY(f_p)] +
		    0.5*g->az * f_p->a[index + DZ(f_p)]
		    + g->b*source(i,j,k, g, p);
//		printf("s=%lf\n",  g->b*source(i,j,k, g, p));
		idx++;
	    }
	}
    }
}

void update_diffusion_matrix_with_field_mg(matrix* m_p, field* f_p, Geometry* g, Parameters* p, int mult, double source)
{
    int i,j,k;
    int idx = 0;
    double m2 = 1./(mult*mult);
//    printf("nx=%d ny=%d nz=%d\n", f_p->nx, f_p->ny, f_p->nz);
    for (k = 1; k<f_p->nz-1; k++)
    {
	for (j = 1; j<f_p->ny-1; j++)
	{
	    for (i = 1; i<f_p->nx-1; i++)
	    {
		int index = INDEX_P(f_p,i,j,k);
		m_p->a[IDX(m_p,idx,m_p->ny-1)] =
		    0.5*g->ax *m2* f_p->a[index - DX(f_p)] +
		    0.5*g->ay *m2* f_p->a[index - DY(f_p)] +
		    0.5*g->az *m2* f_p->a[index - DZ(f_p)] +
		    (1. - g->ax*m2 - g->ay*m2 - g->az*m2) * f_p->a[index] +
		    0.5*g->ax*m2 * f_p->a[index + DX(f_p)] +
		    0.5*g->ay*m2 * f_p->a[index + DY(f_p)] +
		    0.5*g->az*m2 * f_p->a[index + DZ(f_p)]
		    + source*g->b*source_mg(i,j,1, g, p, mult);
//		printf("s=%lf\n",  g->b*source_mg(i,j,k, g, p, mult));
		idx++;
	    }
	}
    }
}

void update_diffusion_matrix_mg(matrix* m_p, field* f_p, Geometry* g, Parameters* p)
{
    int i,j,k;
    int idx = 0;
    
    for (k = 1; k<f_p->nz-1; k++)
    {
	for (j = 1; j<f_p->ny-1; j++)
	{
	    for (i = 1; i<f_p->nx-1; i++)
	    {
		int index = INDEX_P(f_p,i,j,k);
		m_p->a[IDX(m_p,idx,m_p->ny-1)] = f_p->a[index];
		idx++;
	    }
	}
    }
}

void update_advection_matrix_with_field(matrix* m_p, field* f_p, Geometry* g, Parameters* p)
{
    int i,j,k=0;
    int idx = 0;
    for (k = 1; k<g->nz-1; k++)
    {
	for (j = 1; j<g->ny-1; j++)
	{
	    for (i = 1; i<g->nx-1; i++)
	    {
		int index = INDEX_P(f_p,i,j,k);
//		fprintf(stderr, "2 %d %d %d\n", m_p->nx, m_p->ny, IDX(m_p,idx,m_p->ny-1));
		m_p->a[IDX(m_p,idx,m_p->ny-1)] =
		    0.5*g->ax * f_p->a[index - DX(f_p)] +
		    0.5*g->ay * f_p->a[index - DY(f_p)] +
		    0.5*g->az * f_p->a[index - DZ(f_p)] +
		    (1. - g->ax - g->ay - g->az) * f_p->a[index] +
		    0.5*g->ax * f_p->a[index + DX(f_p)] +
		    0.5*g->ay * f_p->a[index + DY(f_p)] +
		    0.5*g->az * f_p->a[index + DZ(f_p)] -
		    0.25*g->bx * f_p->a[index - DX(f_p)] -
		    0.25*g->by * f_p->a[index - DY(f_p)] -
		    0.25*g->bz * f_p->a[index - DZ(f_p)] +
		    0.25*g->bx * f_p->a[index + DX(f_p)] +
		    0.25*g->by * f_p->a[index + DY(f_p)] +
		    0.25*g->bz * f_p->a[index + DZ(f_p)] +
		    g->b*source(i,j,k,g, p);
		idx++;
	    }
	}
    }
}

double norm(double* a1, double* a2, int n)
{
    double ans = 0.;
    int i;
    for (i = 0; i < n; i++)
    {
	ans += (a1[i] - a2[i])*(a1[i] - a2[i]);
    }
    return ans;
}

int sor(matrix* m, double* x, double tol, int nmax, double w)
{
    int nr = m->nx;

    double x1[nr];
    int i,j;
    int n = 0;
    do {
	for (i = 0; i < nr; i++)
	{
	    x1[i] = x[i];
	}
	for (i = 0; i < nr; i++)
	{
	    double var = 0.;
	    for (j = 0; j < i; j++)
	    {
		var += m->a[IDX(m,i,j)] * x[j];
	    }
	    for (j = i + 1; j < nr; j++ )
	    {
		var += m->a[IDX(m,i,j)] * x1[j];
	    }
	    x[i] = (m->a[IDX(m,i,m->ny-1)] - var) / m->a[IDX(m,i,i)];
	    x[i] = (1.0 - w) * x1[i] + w * x[i];
	}
	n++;
    } while (norm(x, x1, nr) > tol && n < nmax);
    return n;
}

int sor_n(matrix* m, double* x, double tol, int nmax, double w)
{
    int nr = m->nx;

    double z[nr];
    //int i,j;
    int n = 0;
    do {
	double z[nr];
	int i,j;
   
	for (i = 0; i < nr; i++)
	{
	    double val = m->a[IDX(m,i,m->ny-1)];
	    for (j=0; j<i; j++)
	    {
		val -= m->a[IDX(m,i,j)]*z[j];
	    }
	    z[i] = val/m->a[IDX(m,i,i)];
	}
	for (i = nr-1; i>=0; i--)
	{
	    double val = z[i];
	    for (j=i+1; j<nr; j++)
	    {
		val -= m->a[IDX(m,i,j)]*x[j]/m->a[IDX(m,i,i)];
	    }
	    x[i] = val;
	}
	n++;
    } while (norm(x, z, nr) > tol && n < nmax);
    return n;
}





